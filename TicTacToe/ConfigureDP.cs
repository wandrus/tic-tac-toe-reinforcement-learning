using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

namespace TicTacToe.Agent
{
	/// <summary>
	/// Summary description for ConfigureDP.
	/// </summary>
	public class ConfigureDP : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown numericUpDown3;
		private System.Windows.Forms.NumericUpDown numericUpDown4;
		private System.Windows.Forms.NumericUpDown numericUpDown5;
		private System.Windows.Forms.Button button3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ConfigureDP()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			try
			{
				XmlTextReader DPconfig = new XmlTextReader("DPConfig.xml");
				
				DPconfig.ReadStartElement("DynamicProgramming");
				DPconfig.ReadStartElement("Epsilon");
				this.numericUpDown1.Value = decimal.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Stepsize");
				DPconfig.ReadString();
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Reward");
				DPconfig.ReadStartElement("Win");
				this.numericUpDown3.Value = int.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Tie");
				this.numericUpDown4.Value = int.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Loss");
				this.numericUpDown5.Value = int.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("AutoExplore");
				string ans = DPconfig.ReadString();
				if(ans == "Yes")
				{
					this.radioButton1.Checked = true;
					this.radioButton2.Checked = false;
				}
				else //ans == no
				{
					this.radioButton1.Checked = false;
					this.radioButton2.Checked = true;
				}
				DPconfig.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				MessageBox.Show(err.ToString(),"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ConfigureDP));
			this.button1 = new System.Windows.Forms.Button();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.button5 = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.button4 = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
			this.button3 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(192, 13);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(104, 20);
			this.button1.TabIndex = 11;
			this.button1.Text = "Help on Epsilon";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.DecimalPlaces = 4;
			this.numericUpDown1.Increment = new System.Decimal(new int[] {
																			 1,
																			 0,
																			 0,
																			 262144});
			this.numericUpDown1.Location = new System.Drawing.Point(104, 13);
			this.numericUpDown1.Maximum = new System.Decimal(new int[] {
																		   1,
																		   0,
																		   0,
																		   0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(64, 20);
			this.numericUpDown1.TabIndex = 10;
			this.numericUpDown1.Value = new System.Decimal(new int[] {
																		 1,
																		 0,
																		 0,
																		 65536});
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 20);
			this.label1.TabIndex = 9;
			this.label1.Text = "Epsilon";
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(240, 208);
			this.button5.Name = "button5";
			this.button5.TabIndex = 17;
			this.button5.Text = "OK";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButton2);
			this.groupBox2.Controls.Add(this.radioButton1);
			this.groupBox2.Controls.Add(this.button4);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(8, 136);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(312, 64);
			this.groupBox2.TabIndex = 16;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Auto Explore New States?";
			// 
			// radioButton2
			// 
			this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.radioButton2.Location = new System.Drawing.Point(80, 32);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(56, 24);
			this.radioButton2.TabIndex = 1;
			this.radioButton2.Text = "No";
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.radioButton1.Location = new System.Drawing.Point(16, 32);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(56, 24);
			this.radioButton1.TabIndex = 0;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Yes";
			// 
			// button4
			// 
			this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button4.Location = new System.Drawing.Point(176, 32);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(128, 20);
			this.button4.TabIndex = 2;
			this.button4.Text = "Help on Auto Explore";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.numericUpDown3);
			this.groupBox1.Controls.Add(this.numericUpDown4);
			this.groupBox1.Controls.Add(this.numericUpDown5);
			this.groupBox1.Controls.Add(this.button3);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(8, 40);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(312, 98);
			this.groupBox1.TabIndex = 15;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Rewards";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(8, 72);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(48, 20);
			this.label5.TabIndex = 4;
			this.label5.Text = "Lost";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(8, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(32, 20);
			this.label4.TabIndex = 2;
			this.label4.Text = "Tie";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 20);
			this.label3.TabIndex = 0;
			this.label3.Text = "Win";
			// 
			// numericUpDown3
			// 
			this.numericUpDown3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.numericUpDown3.Location = new System.Drawing.Point(96, 24);
			this.numericUpDown3.Minimum = new System.Decimal(new int[] {
																		   100,
																		   0,
																		   0,
																		   -2147483648});
			this.numericUpDown3.Name = "numericUpDown3";
			this.numericUpDown3.Size = new System.Drawing.Size(64, 22);
			this.numericUpDown3.TabIndex = 1;
			this.numericUpDown3.Value = new System.Decimal(new int[] {
																		 1,
																		 0,
																		 0,
																		 0});
			// 
			// numericUpDown4
			// 
			this.numericUpDown4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.numericUpDown4.Location = new System.Drawing.Point(96, 48);
			this.numericUpDown4.Minimum = new System.Decimal(new int[] {
																		   100,
																		   0,
																		   0,
																		   -2147483648});
			this.numericUpDown4.Name = "numericUpDown4";
			this.numericUpDown4.Size = new System.Drawing.Size(64, 22);
			this.numericUpDown4.TabIndex = 3;
			// 
			// numericUpDown5
			// 
			this.numericUpDown5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.numericUpDown5.Location = new System.Drawing.Point(96, 72);
			this.numericUpDown5.Minimum = new System.Decimal(new int[] {
																		   100,
																		   0,
																		   0,
																		   -2147483648});
			this.numericUpDown5.Name = "numericUpDown5";
			this.numericUpDown5.Size = new System.Drawing.Size(64, 22);
			this.numericUpDown5.TabIndex = 5;
			this.numericUpDown5.Value = new System.Decimal(new int[] {
																		 1,
																		 0,
																		 0,
																		 -2147483648});
			// 
			// button3
			// 
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button3.Location = new System.Drawing.Point(184, 48);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(104, 20);
			this.button3.TabIndex = 6;
			this.button3.Text = "Help on Rewards";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// ConfigureDP
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(320, 237);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.numericUpDown1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ConfigureDP";
			this.Text = "Dynamic Programming Configuration";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("EPSILON is used for exploitation to set a range of acceptable values.\n"+
				"If there are 2 or more values that are within a acceptable \"range\""+
				"then the agent will randomly select among those\n"+ 
				"eg.  State1 = .9, State2 = .8, State3 = .79\n"+
				"with epsilon at 0.1 it would look at anything within 0.9 - 0.1 = 0.8\n"+
				"\nThe Maximum value of epsilon is 1.0, in this case the possible states are randomly selected.\n"+
				"The Minimum value of epsilon is 0.0, in this case only the highest value would be selected.","Epsilon Information",MessageBoxButtons.OK,MessageBoxIcon.Information);
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("The smaller the STEP SIZE the smarter the agent will play\n"+
				"but then the agent will need to play more games. (Takes a longer time to learn)\n"+
				"I recommended to use a higher STEPSIZE like 0.1 in the beginning of learning\n"+
				"then change the step size to a lower value later on.\n"+ 
				"\nTry 0.1 (Ten visits per state to get a decent value) then\n"+
				"Try 0.01 (One Hundered visits per state) then\n"+
				"Try 0.001 (1000 visits/state) and so on\n"+
				"\nThe step size value deals with the updating of the state's value\n"+
				"The formula is as follow\n"+
				"newValue = oldValue + stepSize * ( REWARD - oldValue)","Step Size Information",MessageBoxButtons.OK,MessageBoxIcon.Information);
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("The \"win\" represents the reward or punishment you will give the agent anytime it wins\n"+
				"The \"tie\" represents the reward or punishment you will give the agent when the game is a tie\n"+
				"The \"lost\" represents the reward or punishment you will give the agent anytime it loses\n"+
				"\nPlaying around with these values can change how well and quickly the agent may learn\n"+
				"By default the agent is given a +1 for a win, a 0 for a tie, and a -1 for a lost\n"+
				"However, some other interesting rewards may be\n"+
				"win: +1, tie: -1, lost:  -1   Here anything but a win is only satisfactory\n"+
				"win: 0, tie: -1, lost: -2     Here the agent learns to fear ties and lost, and recieves no positive reinforcement\n"
				,"Help on Rewards",MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Automatic exploration of new states will allow your agent to learn\n"+
				"more quickly, by visiting states new states no matter what the other\n"+
				"values of the visted states might be. The more states that the agent\n"+
				"knows of, the smarter it will be.","Information on Automatic Exploration of New States",MessageBoxButtons.OK,MessageBoxIcon.Information);
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			try
			{
				XmlTextWriter DPconfig = new XmlTextWriter("DPConfig.xml",System.Text.Encoding.Default);
				DPconfig.WriteStartDocument(true);
				DPconfig.WriteComment("This xml is used to configure the Dynamic Programming learning");
				DPconfig.WriteStartElement("DynamicProgramming");
				DPconfig.WriteStartElement("Epsilon");
				DPconfig.WriteString(this.numericUpDown1.Value.ToString());
				DPconfig.WriteEndElement();
				DPconfig.WriteStartElement("Stepsize");
				DPconfig.WriteString("0.1");//Not needed, but to lazy to remove and fix potential problems
				DPconfig.WriteEndElement();
				DPconfig.WriteStartElement("Reward");
				DPconfig.WriteStartElement("Win");
				DPconfig.WriteString(this.numericUpDown3.Value.ToString());
				DPconfig.WriteEndElement();
				DPconfig.WriteStartElement("Tie");
				DPconfig.WriteString(this.numericUpDown4.Value.ToString());
				DPconfig.WriteEndElement();
				DPconfig.WriteStartElement("Loss");
				DPconfig.WriteString(this.numericUpDown5.Value.ToString());
				DPconfig.WriteEndElement();
				DPconfig.WriteEndElement();
				DPconfig.WriteStartElement("AutoExplore");
				if(this.radioButton1.Checked)
				{
					DPconfig.WriteString("Yes");
				}
				else
				{
					DPconfig.WriteString("No");
				}
				DPconfig.WriteEndElement();
				DPconfig.WriteEndElement();
				DPconfig.WriteEndDocument();
				DPconfig.Close();
			}
			catch(Exception err)
			{
				MessageBox.Show(err.ToString(),"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
			this.Close();
		}


	}
}
