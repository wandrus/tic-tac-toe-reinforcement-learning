using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using System.Xml;
using System.IO;

namespace TicTacToe
{

	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.PictureBox pictureBox1;
		protected int turn = 0;
		protected int[] currentState = new int[9]{0,0,0,0,0,0,0,0,0}; 
		protected int[] previousState = new int[9]{0,0,0,0,0,0,0,0,0};
		protected int win = 0;
		private int gameType = 0;
		private int movesLeft = 9;
		private Agent.DynamicProgramming agentDPO;
		private Agent.DynamicProgramming agentDPX;
		private Agent.MonteCarlo agentMCO;
		private Agent.MonteCarlo agentMCX;
		private Agent.TemporalDifference agentTDO;
		private Agent.TemporalDifference agentTDX;
		private int winner = -1;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem menuItem12;
		private System.Windows.Forms.MenuItem menuItem13;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.MenuItem menuItem15;
		private System.Windows.Forms.MenuItem menuItem16;
		private System.Windows.Forms.MenuItem menuItem17;
		private System.Windows.Forms.MenuItem menuItem19;
		private System.Windows.Forms.MenuItem menuItem18;
		private System.Windows.Forms.MenuItem menuItem20;
		private System.Windows.Forms.MenuItem menuItem21;
		private System.Windows.Forms.MenuItem menuItem22;
		private System.Windows.Forms.MenuItem menuItem23;
		private System.Windows.Forms.MenuItem menuItem24;
		private System.Windows.Forms.MenuItem menuItem25;
		private System.Windows.Forms.MenuItem menuItem26;
		private System.Windows.Forms.MenuItem menuItem27;
		private System.Windows.Forms.MenuItem menuItem28;
		private System.Windows.Forms.MenuItem menuItem29;
		private System.Windows.Forms.MenuItem menuItem30;
		private System.Windows.Forms.MenuItem menuItem31;
		private int count = 1;
		private System.Windows.Forms.StatusBarPanel statusBarPanel1;
		private System.Windows.Forms.MenuItem menuItem32;
		private System.Windows.Forms.MenuItem menuItem33;
		private System.Windows.Forms.MenuItem menuItem34;
		private System.Windows.Forms.MenuItem menuItem35;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			ReadCount();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.menuItem10 = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.menuItem19 = new System.Windows.Forms.MenuItem();
			this.menuItem12 = new System.Windows.Forms.MenuItem();
			this.menuItem13 = new System.Windows.Forms.MenuItem();
			this.menuItem14 = new System.Windows.Forms.MenuItem();
			this.menuItem18 = new System.Windows.Forms.MenuItem();
			this.menuItem20 = new System.Windows.Forms.MenuItem();
			this.menuItem21 = new System.Windows.Forms.MenuItem();
			this.menuItem22 = new System.Windows.Forms.MenuItem();
			this.menuItem31 = new System.Windows.Forms.MenuItem();
			this.menuItem23 = new System.Windows.Forms.MenuItem();
			this.menuItem24 = new System.Windows.Forms.MenuItem();
			this.menuItem25 = new System.Windows.Forms.MenuItem();
			this.menuItem26 = new System.Windows.Forms.MenuItem();
			this.menuItem27 = new System.Windows.Forms.MenuItem();
			this.menuItem28 = new System.Windows.Forms.MenuItem();
			this.menuItem29 = new System.Windows.Forms.MenuItem();
			this.menuItem32 = new System.Windows.Forms.MenuItem();
			this.menuItem15 = new System.Windows.Forms.MenuItem();
			this.menuItem16 = new System.Windows.Forms.MenuItem();
			this.menuItem30 = new System.Windows.Forms.MenuItem();
			this.menuItem17 = new System.Windows.Forms.MenuItem();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.menuItem33 = new System.Windows.Forms.MenuItem();
			this.menuItem34 = new System.Windows.Forms.MenuItem();
			this.menuItem35 = new System.Windows.Forms.MenuItem();
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
			this.SuspendLayout();
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 285);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
																						  this.statusBarPanel1});
			this.statusBar1.ShowPanels = true;
			this.statusBar1.Size = new System.Drawing.Size(352, 22);
			this.statusBar1.SizingGrip = false;
			this.statusBar1.TabIndex = 0;
			this.statusBar1.Text = "Ready";
			// 
			// statusBarPanel1
			// 
			this.statusBarPanel1.Text = "Ready";
			this.statusBarPanel1.Width = 140;
			// 
			// progressBar1
			// 
			this.progressBar1.Enabled = false;
			this.progressBar1.Location = new System.Drawing.Point(144, 287);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(200, 20);
			this.progressBar1.TabIndex = 1;
			this.progressBar1.Visible = false;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem10,
																					  this.menuItem23,
																					  this.menuItem15});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2,
																					  this.menuItem9});
			this.menuItem1.Text = "File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem3});
			this.menuItem2.Text = "&New";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 0;
			this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem4,
																					  this.menuItem5,
																					  this.menuItem8});
			this.menuItem3.Text = "Game";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 0;
			this.menuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem6,
																					  this.menuItem7});
			this.menuItem4.Text = "Single Player";
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 0;
			this.menuItem6.Text = "You as X";
			this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 1;
			this.menuItem7.Text = "You as O";
			this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 1;
			this.menuItem5.Text = "Two Player";
			this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 2;
			this.menuItem8.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem33,
																					  this.menuItem34,
																					  this.menuItem35});
			this.menuItem8.Text = "Simulation";
			this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 1;
			this.menuItem9.Text = "&Close";
			this.menuItem9.Click += new System.EventHandler(this.menuItem9_Click);
			// 
			// menuItem10
			// 
			this.menuItem10.Index = 1;
			this.menuItem10.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem11,
																					   this.menuItem18,
																					   this.menuItem31});
			this.menuItem10.Text = "Edit";
			// 
			// menuItem11
			// 
			this.menuItem11.Index = 0;
			this.menuItem11.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem19});
			this.menuItem11.Text = "Agent";
			// 
			// menuItem19
			// 
			this.menuItem19.Index = 0;
			this.menuItem19.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem12,
																					   this.menuItem13,
																					   this.menuItem14});
			this.menuItem19.Text = "Learning Style";
			// 
			// menuItem12
			// 
			this.menuItem12.Index = 0;
			this.menuItem12.Text = "Dynamic Programming";
			this.menuItem12.Click += new System.EventHandler(this.menuItem12_Click);
			// 
			// menuItem13
			// 
			this.menuItem13.Index = 1;
			this.menuItem13.Text = "Monte Carlo";
			this.menuItem13.Click += new System.EventHandler(this.menuItem13_Click);
			// 
			// menuItem14
			// 
			this.menuItem14.Index = 2;
			this.menuItem14.Text = "Temporal Difference";
			this.menuItem14.Click += new System.EventHandler(this.menuItem14_Click);
			// 
			// menuItem18
			// 
			this.menuItem18.Index = 1;
			this.menuItem18.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem20,
																					   this.menuItem21,
																					   this.menuItem22});
			this.menuItem18.Text = "Opponent";
			// 
			// menuItem20
			// 
			this.menuItem20.Index = 0;
			this.menuItem20.Text = "Dynamic Programming Agent";
			this.menuItem20.Click += new System.EventHandler(this.menuItem20_Click);
			// 
			// menuItem21
			// 
			this.menuItem21.Checked = true;
			this.menuItem21.DefaultItem = true;
			this.menuItem21.Index = 1;
			this.menuItem21.Text = "Monte Carlo Agent";
			this.menuItem21.Click += new System.EventHandler(this.menuItem21_Click);
			// 
			// menuItem22
			// 
			this.menuItem22.Index = 2;
			this.menuItem22.Text = "Temporal Difference Agent";
			this.menuItem22.Click += new System.EventHandler(this.menuItem22_Click);
			// 
			// menuItem31
			// 
			this.menuItem31.Index = 2;
			this.menuItem31.Text = "Watch Count";
			this.menuItem31.Click += new System.EventHandler(this.menuItem31_Click);
			// 
			// menuItem23
			// 
			this.menuItem23.Index = 2;
			this.menuItem23.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem24,
																					   this.menuItem28,
																					   this.menuItem29,
																					   this.menuItem32});
			this.menuItem23.Text = "Data";
			// 
			// menuItem24
			// 
			this.menuItem24.Index = 0;
			this.menuItem24.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem25,
																					   this.menuItem26,
																					   this.menuItem27});
			this.menuItem24.Text = "XML";
			// 
			// menuItem25
			// 
			this.menuItem25.Index = 0;
			this.menuItem25.Text = "Dynamic Programming";
			this.menuItem25.Click += new System.EventHandler(this.menuItem25_Click);
			// 
			// menuItem26
			// 
			this.menuItem26.Index = 1;
			this.menuItem26.Text = "Monte Carlo";
			this.menuItem26.Click += new System.EventHandler(this.menuItem26_Click);
			// 
			// menuItem27
			// 
			this.menuItem27.Index = 2;
			this.menuItem27.Text = "Temporal Difference";
			this.menuItem27.Click += new System.EventHandler(this.menuItem27_Click);
			// 
			// menuItem28
			// 
			this.menuItem28.Index = 1;
			this.menuItem28.Text = "State";
			this.menuItem28.Click += new System.EventHandler(this.menuItem28_Click);
			// 
			// menuItem29
			// 
			this.menuItem29.Index = 2;
			this.menuItem29.Text = "Win Percentage";
			this.menuItem29.Click += new System.EventHandler(this.menuItem29_Click);
			// 
			// menuItem32
			// 
			this.menuItem32.Index = 3;
			this.menuItem32.Text = "Total States";
			this.menuItem32.Click += new System.EventHandler(this.menuItem32_Click);
			// 
			// menuItem15
			// 
			this.menuItem15.Index = 3;
			this.menuItem15.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem16,
																					   this.menuItem30,
																					   this.menuItem17});
			this.menuItem15.Text = "Help";
			// 
			// menuItem16
			// 
			this.menuItem16.Index = 0;
			this.menuItem16.Text = "How To Play";
			this.menuItem16.Click += new System.EventHandler(this.menuItem16_Click);
			// 
			// menuItem30
			// 
			this.menuItem30.Index = 1;
			this.menuItem30.Text = "Reinforcement Learning Info";
			this.menuItem30.Click += new System.EventHandler(this.menuItem30_Click);
			// 
			// menuItem17
			// 
			this.menuItem17.Index = 2;
			this.menuItem17.Text = "&About";
			this.menuItem17.Click += new System.EventHandler(this.menuItem17_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.White;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(352, 285);
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawGameBoard);
			this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
			// 
			// menuItem33
			// 
			this.menuItem33.Index = 0;
			this.menuItem33.Text = "Against Dynamical Programming";
			this.menuItem33.Click += new System.EventHandler(this.menuItem33_Click);
			// 
			// menuItem34
			// 
			this.menuItem34.Checked = true;
			this.menuItem34.Index = 1;
			this.menuItem34.Text = "Against Monte Carlo";
			this.menuItem34.Click += new System.EventHandler(this.menuItem34_Click);
			// 
			// menuItem35
			// 
			this.menuItem35.Index = 2;
			this.menuItem35.Text = "Against Temporal Difference";
			this.menuItem35.Click += new System.EventHandler(this.menuItem35_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(352, 307);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.statusBar1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.Text = "Tic Tac Toe";
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void menuItem9_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void ReadCount()
		{
			try
			{
				XmlTextReader countReader = new XmlTextReader("Interplay.xml");
				countReader.ReadStartElement("count");
				count = int.Parse(countReader.ReadString());
				countReader.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				XmlTextWriter counter = new XmlTextWriter("Interplay.xml",System.Text.Encoding.Default);
				counter.WriteStartDocument(true);
				counter.WriteComment("Just a small xml, dealing with the number of times a computer vs. computer would play");
				counter.WriteStartElement("count");
				counter.WriteString("1");
				counter.WriteEndElement();
				counter.WriteEndDocument();
				counter.Close();
			}
		}

		//Single Player, User as X
		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			this.turn = 0;
			movesLeft = 9;
			win = 0;
			gameType = 1;
			currentState = new int[9]{0,0,0,0,0,0,0,0,0};
			User.User userX = new User.User();
			if(this.menuItem20.Checked)
			{
				agentDPO = new Agent.DynamicProgramming(2);
			}
			else if(this.menuItem21.Checked)
			{
				agentMCO = new Agent.MonteCarlo(2);
			}
			else //this menuItem22.Checked
			{
				agentTDO = new Agent.TemporalDifference(2);
			}
			this.pictureBox1.Invalidate();
		}

		//Single Player, User as O
		private void menuItem7_Click(object sender, System.EventArgs e)
		{
			this.turn = 0;
			movesLeft = 9;
			win = 0;
			gameType = 2;
			currentState = new int[9]{0,0,0,0,0,0,0,0,0};
			if(this.menuItem20.Checked)
			{
				agentDPX = new Agent.DynamicProgramming(1);
				currentState[agentDPX.getMove(currentState,9)] = 1;
				turn = 1;
			}
			else if(this.menuItem21.Checked)
			{
				agentMCX = new Agent.MonteCarlo(1);
				currentState[agentMCX.getMove(currentState,9)] = 1;
				turn = 1;
			}
			else //this menuItem22.Checked
			{
				agentTDX = new Agent.TemporalDifference(1);
				currentState[agentTDX.getMove(currentState,9)] = 1;
				turn = 1;
			}
			User.User userO = new User.User();
			this.pictureBox1.Invalidate();
		}

		//Two Player Game
		private void menuItem5_Click(object sender, System.EventArgs e)
		{
			this.turn = 0;
			movesLeft = 9;
			win = 0;
			gameType = 3;
			currentState = new int[9]{0,0,0,0,0,0,0,0,0};
			User.User userO = new User.User();
			User.User userX = new User.User();
			this.pictureBox1.Invalidate();
		}

		//Computer vs. Computer
		private void menuItem8_Click(object sender, System.EventArgs e)
		{
			ReadCount();
			
			this.progressBar1.Maximum = count;
			this.progressBar1.Step = 1;
			this.progressBar1.Show();
			this.progressBar1.Value = 0;
			this.statusBarPanel1.Text = "Running Simulations";
			this.statusBar1.Invalidate();
			for(int j = 0; j<count; j++)
			{
				this.progressBar1.Increment(this.progressBar1.Step);
				this.pictureBox1.Invalidate();
				movesLeft = 9;
				win = 0;
				gameType = 4;
				currentState = new int[9]{0,0,0,0,0,0,0,0,0};

				if(this.menuItem20.Checked)
				{
					agentDPX = new Agent.DynamicProgramming(1);
					if(this.menuItem34.Checked)
					{
						agentMCO = new Agent.MonteCarlo(2);
					}
					else if(this.menuItem35.Checked)
					{
						agentTDO = new Agent.TemporalDifference(2);
					}
					else//(this.menuItem33.Checked || default)
					{
						agentDPO = new Agent.DynamicProgramming(2);
					}

					bool turnBase = true;

					while(this.win == 0)
					{
						if(turnBase)
						{
							turnBase = false;
							for(int i = 0; i < 9; i++)
							{
								previousState[i] = currentState[i];
							}
							currentState[agentDPX.getMove(currentState,9)] = 1;
							turn = (turn+1)%2;
							checkWin();
							this.movesLeft--;
							this.pictureBox1.Invalidate();
						}
						else
						{
							turnBase = true;
							for(int i = 0; i < 9; i++)
							{
								previousState[i] = currentState[i];
							}
							if(this.menuItem34.Checked)
							{
								currentState[agentMCO.getMove(currentState,9)] = 2;
							}
							else if(this.menuItem35.Checked)
							{
								currentState[agentTDO.getMove(currentState,9)] = 2;
							}
							else
							{
								currentState[agentDPO.getMove(currentState,9)] = 2;
							}
							turn = (turn+1)%2;
							checkWin();
							this.movesLeft--;
							this.pictureBox1.Invalidate();
						}
					}
					if(winner == 1)//X won
					{
						agentDPX.update(1,currentState);
						if(this.menuItem34.Checked)
						{
							agentMCO.update(-1,4);
						}
						else if(this.menuItem35.Checked)
						{
							agentTDO.update(-1,previousState);
						}
						else
						{
							agentDPO.update(-1,previousState);
						}
					}
					else if(winner == 2)//O won
					{
						agentDPX.update(-1,previousState);
						if(this.menuItem34.Checked)
						{
							agentMCO.update(1,4);
						}
						else if(this.menuItem35.Checked)
						{
							agentTDO.update(1,currentState);
						}
						else
						{
							agentDPO.update(1,currentState);
						}
						
					}
					else //Cat's game
					{
						if(turn == 0)
						{
							agentDPX.update(0,previousState);
							if(this.menuItem34.Checked)
							{
								agentMCO.update(0,4);
							}
							else if(this.menuItem35.Checked)
							{
								agentTDO.update(0,currentState);
							}
							else
							{
								agentDPO.update(0,currentState);
							}
						}
						else //turn ==1
						{
							agentDPX.update(0,currentState);
							if(this.menuItem34.Checked)
							{
								agentMCO.update(0,4);
							}
							else if(this.menuItem35.Checked)
							{
								agentTDO.update(0,previousState);
							}
							else
							{
								agentDPO.update(0,previousState);
							}
							
						}
					}
				}
				else if(this.menuItem21.Checked)
				{
					agentMCX = new Agent.MonteCarlo(1);
					if(this.menuItem33.Checked)
					{
						agentDPO = new Agent.DynamicProgramming(2);
					}
					else if(this.menuItem35.Checked)
					{
						agentTDO = new Agent.TemporalDifference(2);
					}
					else//(this.menuItem34.Checked || default)
					{
						agentMCO = new Agent.MonteCarlo(2);
					}
				
					bool turnBase = true;
					while(this.win == 0)
					{
						if(turnBase)
						{
							turnBase = false;
							currentState[agentMCX.getMove(currentState,9)] = 1;
							turn = (turn+1)%2;
							checkWin();
							this.movesLeft--;
							this.pictureBox1.Invalidate();
						

						}
						else
						{
							turnBase = true;
							for(int i = 0; i < 9; i++)
							{
								previousState[i] = currentState[i];
							}

							if(this.menuItem33.Checked)
							{
								currentState[agentDPO.getMove(currentState,9)] = 2;
							}
							else if(this.menuItem35.Checked)
							{
								currentState[agentTDO.getMove(currentState,9)] = 2;
							}
							else
							{
								currentState[agentMCO.getMove(currentState,9)] = 2;
							}
							turn = (turn+1)%2;
							checkWin();
							this.movesLeft--;
							this.pictureBox1.Invalidate();
						
						}
					}
					if(winner == 1)//X won
					{
						agentMCX.update(1,4);
						if(this.menuItem33.Checked)
						{
							agentDPO.update(-1,previousState);
						}
						else if(this.menuItem35.Checked)
						{
							agentTDO.update(-1,previousState);
						}
						else
						{
							agentMCO.update(-1,4);
						}
					}
					else if(winner == 2)//O won
					{
						agentMCX.update(-1,4);
						if(this.menuItem33.Checked)
						{
							agentDPO.update(1,currentState);
						}
						else if(this.menuItem35.Checked)
						{
							agentTDO.update(1,currentState);
						}
						else
						{
							agentMCO.update(1,4);
						}
					}
					else //Cat's game
					{
						agentMCX.update(0,4);
						if(this.menuItem33.Checked)
						{
							if(turn == 0)
							{
								agentDPO.update(0,currentState);
							}
							else
							{
								agentDPO.update(0,previousState);
							}
						}
						else if(this.menuItem35.Checked)
						{
							if(turn == 0)
							{
								agentTDO.update(0,currentState);
							}
							else
							{
								agentTDO.update(0,previousState);
							}
						}
						else
						{
							agentMCO.update(0,4);
						}
					}
				}
				else //this menuItem22.Checked
				{
					agentTDX = new Agent.TemporalDifference(1);
					if(this.menuItem33.Checked)
					{
						agentDPO = new Agent.DynamicProgramming(2);
					}
					else if(this.menuItem34.Checked)
					{
						agentMCO = new Agent.MonteCarlo(2);
					}
					else//(this.menuItem35.Checked || default)
					{
						agentTDO = new Agent.TemporalDifference(2);
					}
					

					bool turnBase = true;

					while(this.win == 0)
					{
						if(turnBase)
						{
							turnBase = false;
							for(int i = 0; i < 9; i++)
							{
								previousState[i] = currentState[i];
							}
							currentState[agentTDX.getMove(currentState,9)] = 1;
							turn = (turn+1)%2;
							checkWin();
							this.movesLeft--;
							this.pictureBox1.Invalidate();
						}
						else
						{
							turnBase = true;
							for(int i = 0; i < 9; i++)
							{
								previousState[i] = currentState[i];
							}
							if(this.menuItem33.Checked)
							{
								currentState[agentDPO.getMove(currentState,9)] = 2;
							}
							else if(this.menuItem34.Checked)
							{
								currentState[agentMCO.getMove(currentState,9)] = 2;
							}
							else
							{
								currentState[agentTDO.getMove(currentState,9)] = 2;
							}
							turn = (turn+1)%2;
							checkWin();
							this.movesLeft--;
							this.pictureBox1.Invalidate();
						}
					}
					if(winner == 1)//X won
					{
						agentTDX.update(1,currentState);
						if(this.menuItem33.Checked)
						{
							agentDPO.update(-1,previousState);
						}
						else if(this.menuItem34.Checked)
						{
							agentMCO.update(-1,4);
						}
						else
						{
							agentTDO.update(-1,previousState);
						}
					}
					else if(winner == 2)//O won
					{
						agentTDX.update(-1,previousState);
						if(this.menuItem33.Checked)
						{
							agentDPO.update(1,currentState);
						}
						else if(this.menuItem34.Checked)
						{
							agentMCO.update(1,4);
						}
						else
						{
							agentTDO.update(1,currentState);
						}
					}
					else //Cat's game
					{
						if(turn == 0)
						{
							agentTDX.update(0,previousState);
							if(this.menuItem33.Checked)
							{
								agentDPO.update(0,currentState);
							}
							else if(this.menuItem34.Checked)
							{
								agentMCO.update(0,4);
							}
							else
							{
								agentTDO.update(0,currentState);
							}
							
						}
						else //turn ==1
						{
							agentTDX.update(0,currentState);
							if(this.menuItem33.Checked)
							{
								agentDPO.update(0,previousState);
							}
							else if(this.menuItem34.Checked)
							{
								agentMCO.update(0,4);
							}
							else
							{
								agentTDO.update(0,previousState);
							}					
						}
					}
				}
				this.pictureBox1.Invalidate();
			}
			this.statusBarPanel1.Text = "Done";
			this.progressBar1.Hide();
			this.statusBar1.Invalidate();
		}

		private void DrawGameBoard(object sender, System.Windows.Forms.PaintEventArgs e)
		{
				Graphics g = e.Graphics;
				//X will be blue
				Brush blueBrush = new SolidBrush(Color.Blue);
				Pen bluePen = new Pen(blueBrush,2.0F);
				//O will be red
				Brush redBrush = new SolidBrush(Color.Red);
				Pen redPen = new Pen(redBrush,2.0F);
				//Board
				Brush blackBrush = new SolidBrush(Color.Black);
				Pen blackPen = new Pen(blackBrush,3.0F);
				//Board Vertical Lines
				g.DrawLine(blackPen,pictureBox1.Width/3,0,pictureBox1.Width/3,pictureBox1.Height);
				g.DrawLine(blackPen,2*pictureBox1.Width/3,0,2*pictureBox1.Width/3,pictureBox1.Height);
				//Board Horizontal Lines
				g.DrawLine(blackPen,0,pictureBox1.Height/3,pictureBox1.Width,pictureBox1.Height/3);
				g.DrawLine(blackPen,0,2*pictureBox1.Height/3,pictureBox1.Width,2*pictureBox1.Height/3);
				//Win Location
				Brush greenBrush = new SolidBrush(Color.Green);
				Pen greenPen = new Pen(greenBrush,3.0F);
				//Cat Brush
				Brush catBrush = new LinearGradientBrush(new Point(0,0),new Point(this.Width,this.Height),Color.Red, Color.Blue);

				//upperleft corner
				if(currentState[0] == 1)
				{
					//draw X
					g.DrawLine(bluePen,0,0,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
					g.DrawLine(bluePen,0,this.pictureBox1.Height/3,this.pictureBox1.Width/3,0);
				}
				else if(currentState[0] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,0,0,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[0] == 0
				{
					//do nothing
				}

				//top middle
				if(currentState[1] == 1)
				{
					//draw X
					g.DrawLine(bluePen,this.pictureBox1.Width/3,0,2*this.pictureBox1.Width/3,this.pictureBox1.Height/3);
					g.DrawLine(bluePen,this.pictureBox1.Width/3,this.pictureBox1.Height/3,2*this.pictureBox1.Width/3,0);
				}
				else if(currentState[1] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,this.pictureBox1.Width/3,0,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[0] == 0
				{
					//do nothing
				}

				//upperright 
				if(currentState[2] == 1)
				{
					//draw X
					g.DrawLine(bluePen,2 * this.pictureBox1.Width/3,0,this.pictureBox1.Width,this.pictureBox1.Height/3);
					g.DrawLine(bluePen,2 * this.pictureBox1.Width/3,this.pictureBox1.Height/3,this.pictureBox1.Width,0);
				}
				else if(currentState[2] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,2 * this.pictureBox1.Width/3,0,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[2] == 0
				{
					//do nothing
				}

				//left-middle
				if(currentState[3] == 1)
				{
					//draw X
					g.DrawLine(bluePen,0,this.pictureBox1.Height/3,this.pictureBox1.Width/3,2 * this.pictureBox1.Height/3);
					g.DrawLine(bluePen,0,2 * this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else if(currentState[3] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,0,this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[3] == 0
				{
					//do nothing
				}

				//center
				if(currentState[4] == 1)
				{
					//draw X
					g.DrawLine(bluePen,this.pictureBox1.Width/3,this.pictureBox1.Height/3,2 * this.pictureBox1.Width/3,2 * this.pictureBox1.Height/3);
					g.DrawLine(bluePen,this.pictureBox1.Width/3,2 * this.pictureBox1.Height/3,2*this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else if(currentState[4] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,this.pictureBox1.Width/3,this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[3] == 0
				{
					//do nothing
				}

				//right-middle
				if(currentState[5] == 1)
				{
					//draw X
					g.DrawLine(bluePen,2*this.pictureBox1.Width/3,this.pictureBox1.Height/3,this.pictureBox1.Width,2 * this.pictureBox1.Height/3);
					g.DrawLine(bluePen,2*this.pictureBox1.Width/3,2 * this.pictureBox1.Height/3,this.pictureBox1.Width,this.pictureBox1.Height/3);
				}
				else if(currentState[5] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,2*this.pictureBox1.Width/3,this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[3] == 0
				{
					//do nothing
				}

				//bottom-left
				if(currentState[6] == 1)
				{
					//draw X
					g.DrawLine(bluePen,0,2*this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height);
					g.DrawLine(bluePen,0,this.pictureBox1.Height,this.pictureBox1.Width/3,2*this.pictureBox1.Height/3);
				}
				else if(currentState[6] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,0,2*this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[6] == 0
				{
					//do nothing
				}

				//bottom-middle
				if(currentState[7] == 1)
				{
					//draw X
					g.DrawLine(bluePen,this.pictureBox1.Width/3,2*this.pictureBox1.Height/3,2*this.pictureBox1.Width/3,this.pictureBox1.Height);
					g.DrawLine(bluePen,this.pictureBox1.Width/3,this.pictureBox1.Height,2*this.pictureBox1.Width/3,2*this.pictureBox1.Height/3);
				}
				else if(currentState[7] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,this.pictureBox1.Width/3,2*this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[7] == 0
				{
					//do nothing
				}

				//bottom-right
				if(currentState[8] == 1)
				{
					//draw X
					g.DrawLine(bluePen,2*this.pictureBox1.Width/3,2*this.pictureBox1.Height/3,this.pictureBox1.Width,this.pictureBox1.Height);
					g.DrawLine(bluePen,2*this.pictureBox1.Width/3,this.pictureBox1.Height,this.pictureBox1.Width,2*this.pictureBox1.Height/3);
				}
				else if(currentState[8] == 2)
				{
					//draw O
					g.DrawEllipse(redPen,2*this.pictureBox1.Width/3,2*this.pictureBox1.Height/3,this.pictureBox1.Width/3,this.pictureBox1.Height/3);
				}
				else //currentState[8] == 0
				{
					//do nothing
				}



				//Win Info
				if(win == 1) //top horizontal
				{
					g.DrawLine(greenPen,0,(this.pictureBox1.Height/3 + 0) /2, this.pictureBox1.Width, (this.pictureBox1.Height/3 + 0) /2);
				}
				else if(win == 2) //middle horizontal
				{
					g.DrawLine(greenPen,0,(2*this.pictureBox1.Height/3 + this.pictureBox1.Height/3) /2, this.pictureBox1.Width, (2*this.pictureBox1.Height/3 + this.pictureBox1.Height/3) /2);
				}
				else if(win == 3) //bottom horizontal
				{
					g.DrawLine(greenPen,0,(2*this.pictureBox1.Height/3 + this.pictureBox1.Height) /2, this.pictureBox1.Width, (2*this.pictureBox1.Height/3 + this.pictureBox1.Height) /2);
				}
				else if(win == 4) //left vertical
				{
					g.DrawLine(greenPen,(this.pictureBox1.Width/3 + 0)/2,0, (this.pictureBox1.Width/3 + 0)/2, this.pictureBox1.Height);
				}
				else if(win == 5) //middle vertical
				{
					g.DrawLine(greenPen,(this.pictureBox1.Width/3 +  2* this.pictureBox1.Width/3)/2,0, (this.pictureBox1.Width/3 + 2*this.pictureBox1.Width/3)/2, this.pictureBox1.Height);
				}
				else if(win == 6) //right vertical
				{
					g.DrawLine(greenPen,(this.pictureBox1.Width +  2* this.pictureBox1.Width/3)/2,0, (this.pictureBox1.Width + 2*this.pictureBox1.Width/3)/2, this.pictureBox1.Height);
				}
				else if(win == 7) //top left to bottom right diagonal
				{
					g.DrawLine(greenPen,0,0,this.pictureBox1.Width, this.pictureBox1.Height);
				}
				else if(win == 8) //bottom left to upper right
				{
					g.DrawLine(greenPen,0,this.pictureBox1.Height,this.pictureBox1.Width,0);
				}
				else if(win == -1) //cats game
				{
					Font font = new Font("Century Gothic",40F);
					g.DrawString("Cat's Game",font,catBrush,0,this.pictureBox1.Height/3);
				}
				else
				{
					//do nothing
				}

				//Clean Up Time
				redPen.Dispose();
				bluePen.Dispose();
				greenPen.Dispose();
				blackPen.Dispose();
			
				redBrush.Dispose();
				blueBrush.Dispose();
				greenBrush.Dispose();
				catBrush.Dispose();
				blackBrush.Dispose();
		}

		private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			int x;
			int y;
			int location;

			
			if(gameType != 0)
			{
					#region Get Location of Click
					//checks to find what column the click was made in
					//else gives an error
					if(e.X <= this.pictureBox1.Width/3 && e.X >= 0)
					{
						x = 0;
					}
					else if(e.X <= 2*this.pictureBox1.Width/3)
					{
						x = 1;
					}
					else if(e.X <= this.pictureBox1.Width)
					{
						x = 2;
					}
					else
					{
						x = -1;
					}

					//checks to find what row the click was made in
					//else gives an error
					if(e.Y <= this.pictureBox1.Height/3 && e.Y >= 0)
					{
						y = 0;
					}
					else if(e.Y <= 2*this.pictureBox1.Height/3)
					{
						y = 1;
					}
					else if(e.Y <= this.pictureBox1.Height)
					{
						y = 2;
					}
					else
					{
						y = -1;
					}


					switch(x)
					{
						case 0:
							if(y==0){location = 0;}
							else if(y==1){location = 3;}
							else if(y==2){location = 6;}
							else{location = -1;}
							break;
						case 1:
							if(y==0){location = 1;}
							else if(y==1){location = 4;}
							else if(y==2){location = 7;}
							else{location = -1;}
							break;
						case 2:
							if(y==0){location = 2;}
							else if(y==1){location = 5;}
							else if(y==2){location = 8;}
							else{location = -1;}
							break;
						case -1:
							location = -1;
							break;
						default:
							location = -1;
							break;
					}
				#endregion
				if(currentState[location] == 0)//check for legal move
				{
					if(win == 0)
					{
						if(turn == 0) //if it's was X 
						{
							currentState[location] = 1;
						}
						else //turn == 1 //if it was O
						{
							currentState[location] = 2;
						}
			
						//MessageBox.Show(location.ToString());
			
						movesLeft--;
						checkWin();
						this.pictureBox1.Invalidate();
						turn = (turn+1)%2;
						
					}
				}
				else
				{
					if(win == 0)
					{
						//A 3-4-3 Hiaku, by accident. <^__^>
						MessageBox.Show("Silly Bear, You can't move there, try else where!","Illegal Move Attempted", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					else
					{
						//agentO
						if(this.gameType == 1)
						{
							if(this.menuItem20.Checked)
							{
								//agentDPO;
								if(winner == 1)//X won
								{
									agentDPO.update(-1,this.currentState);
									//
								}
								else if(winner == 2)//O won
								{
									agentDPO.update(1,this.currentState);
								}
								else //Cat's game
								{
									agentDPO.update(0,this.currentState);
								}								
							}
							else if(this.menuItem21.Checked)
							{
								//agentMCO;
								if(winner == 1)//X won
								{
									agentMCO.update(-1,this.gameType);
									//
								}
								else if(winner == 2)//O won
								{
									agentMCO.update(1,this.gameType);
								}
								else //Cat's game
								{
									agentMCO.update(0,this.gameType);
								}
								
							}
							else //this menuItem22.Checked
							{
								//agentTDO;
								if(winner == 1)//X won
								{
									agentTDO.update(-1,this.currentState);
									//
								}
								else if(winner == 2)//O won
								{
									agentTDO.update(1,this.currentState);
								}
								else //Cat's game
								{
									agentTDO.update(0,this.currentState);
								}	
							}
						}
							//agentX
						else if(this.gameType ==2)
						{
							if(this.menuItem20.Checked)
							{
								//agentDPX;
								if(winner == 1)//X won
								{
									agentDPX.update(1,this.currentState);
									//
								}
								else if(winner == 2)//O won
								{
									agentDPX.update(-1,this.currentState);
								}
								else //Cat's game
								{
									agentDPX.update(0,this.currentState);
								}
							}
							else if(this.menuItem21.Checked)
							{
								//agentMCX;
								if(winner == 1)//X won
								{
									agentMCX.update(1,this.gameType);
									//
								}
								else if(winner == 2)//O won
								{
									agentMCX.update(-11,this.gameType);
								}
								else //Cat's game
								{
									agentMCX.update(0,this.gameType);
								}
								
							}
							else //this menuItem22.Checked
							{
								//agentTDX;
								if(this.menuItem20.Checked)
								{
									//agentDPX;
									if(winner == 1)//X won
									{
										agentTDX.update(1,this.currentState);
										//
									}
									else if(winner == 2)//O won
									{
										agentTDX.update(-1,this.currentState);
									}
									else //Cat's game
									{
										agentTDX.update(0,this.currentState);
									}
								}
							}
						}
						else
						{
						}
						playAgain(sender, e);
					}
				}
			}
			//agentO
			if(this.gameType == 1 && this.movesLeft != 9 && win == 0 && turn == 1)
			{
				if(this.menuItem20.Checked)
				{
					//agentDPO;
					this.currentState[agentDPO.getMove(this.currentState,9)] = 2;
					this.turn = (this.turn + 1)%2;
					movesLeft--;
					checkWin();
					this.pictureBox1.Invalidate();
				}
				else if(this.menuItem21.Checked)
				{
					//agentMCO;
					this.currentState[agentMCO.getMove(this.currentState,9)] = 2;
					this.turn = (this.turn + 1)%2;
					movesLeft--;
					checkWin();
					this.pictureBox1.Invalidate();
				}
				else //this menuItem22.Checked
				{
					//agentTDO;
					this.currentState[agentTDO.getMove(this.currentState,9)] = 2;
					this.turn = (this.turn + 1)%2;
					movesLeft--;
					checkWin();
					this.pictureBox1.Invalidate();
				}
			}
				//agentX
			else if(this.gameType ==2 && win == 0 && turn == 0)
			{
				if(this.menuItem20.Checked)
				{
					//agentDPX;
					this.currentState[agentDPX.getMove(this.currentState,9)] = 1;
					this.turn = (this.turn + 1)%2;
					movesLeft--;
					checkWin();
					this.pictureBox1.Invalidate();
				}
				else if(this.menuItem21.Checked)
				{
					//agentMCO;
					this.currentState[agentMCX.getMove(this.currentState,9)] = 1;
					this.turn = (this.turn + 1)%2;
					movesLeft--;
					checkWin();
					this.pictureBox1.Invalidate();
				}
				else //this menuItem22.Checked
				{
					//agentTDX;
					this.currentState[agentTDX.getMove(this.currentState,9)] = 1;
					this.turn = (this.turn + 1)%2;
					movesLeft--;
					checkWin();
					this.pictureBox1.Invalidate();
				}	
			}
			else
			{
			}
		}
		private void playAgain(object sender,MouseEventArgs e )
		{
			if(DialogResult.Yes == MessageBox.Show("New Game?","Game Over",MessageBoxButtons.YesNo,MessageBoxIcon.Question))
			{
				switch(gameType)
				{
					case 1:
						menuItem6_Click(sender,e);
						break;
					case 2:
						menuItem7_Click(sender,e);
						break;
					case 3:
						menuItem5_Click(sender,e);
						break;
					case 4:
						menuItem8_Click(sender,e);
						break;
					default:
						break;
				}
			}
			else
			{
				//do nothing
			}
		}


		private void checkWin()
		{
			// X wins top horizontal
			if(currentState[0] == 1 && currentState[1] == 1 && currentState[2] == 1)
			{
				win = 1;
				winner = 1;
				winPercentage();
			}
				// O wins top horizontal
			else if(currentState[0] == 2 && currentState[1] == 2 && currentState[2] == 2)
			{
				win = 1;
				winner = 2;
				winPercentage();
			}

				// X wins middle horizontal
			else if(currentState[3] == 1 && currentState[4] == 1 && currentState[5] == 1)
			{
				win = 2;
				winner = 1;
				winPercentage();
			}
				// O wins middle horizontal
			else if(currentState[3] == 2 && currentState[4] == 2 && currentState[5] == 2)
			{
				win = 2;
				winner = 2;
				winPercentage();
			}

				// X wins bottom horizontal
			else if(currentState[6] == 1 && currentState[7] == 1 && currentState[8] == 1)
			{
				win = 3;
				winner = 1;
				winPercentage();
			}
				// O wins bottom horizontal
			else if(currentState[6] == 2 && currentState[7] == 2 && currentState[8] == 2)
			{
				win = 3;
				winner = 2;
				winPercentage();
			}

				// X wins left vertical
			else if(currentState[0] == 1 && currentState[3] == 1 && currentState[6] == 1)
			{
				win = 4;
				winner = 1;
				winPercentage();
			}
				// O wins bottom horizontal
			else if(currentState[0] == 2 && currentState[3] == 2 && currentState[6] == 2)
			{
				win = 4;
				winner = 2;
				winPercentage();
			}

				// X wins middle vertical
			else if(currentState[1] == 1 && currentState[4] == 1 && currentState[7] == 1)
			{
				win = 5;
				winner = 1;
				winPercentage();
			}
				// O wins middle horizontal
			else if(currentState[1] == 2 && currentState[4] == 2 && currentState[7] == 2)
			{
				win = 5;
				winner = 2;
				winPercentage();
			}

				// X wins right vertical
			else if(currentState[2] == 1 && currentState[5] == 1 && currentState[8] == 1)
			{
				win = 6;
				winner = 1;
				winPercentage();
			}
				// O wins right horizontal
			else if(currentState[2] == 2 && currentState[5] == 2 && currentState[8] == 2)
			{
				win = 6;
				winner = 2;
				winPercentage();
			}

				// X wins top left to bottom right diagonal
			else if(currentState[0] == 1 && currentState[4] == 1 && currentState[8] == 1)
			{
				win = 7;
				winner = 1;
				winPercentage();
			}
				// O wins top left to bottom right diagonal
			else if(currentState[0] == 2 && currentState[4] == 2 && currentState[8] == 2)
			{
				win = 7;
				winner = 2;
				winPercentage();
			}

				// X wins bottom left to top right diagonal
			else if(currentState[6] == 1 && currentState[4] == 1 && currentState[2] == 1)
			{
				win = 8;
				winner = 1;
				winPercentage();
			}
				// O wins bottom left to top right diagonal
			else if(currentState[6] == 2 && currentState[4] == 2 && currentState[2] == 2)
			{
				win = 8;
				winner = 2;
				winPercentage();
			}
			else if(currentState[0] != 0 && currentState[1] != 0 && currentState[2] != 0 &&
					currentState[3] != 0 && currentState[4] != 0 && currentState[5] != 0 &&
					currentState[6] != 0 && currentState[7] != 0 && currentState[8] != 0)
			{
				win = -1;
				winner = 0;
				winPercentage();
			}
			else
			{
				//do nothing game not over yet
			}
		}

		private void winPercentage()
		{
			//Read info
			/* <Win>
			 * <DPX>
			 * <Won></Won>
			 * <Played></Played>
			 * </DPX>
			 * ...
			 * </Win>
			 */
			int [,] data = new int[2,8];
			
			try
			{
				XmlTextReader win = new XmlTextReader("WinPercent.xml");
				
				win.ReadStartElement("Win");
				
				win.ReadStartElement("DPX");
				win.ReadStartElement("Won");
				data[0,0] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,0] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("DPO");
				win.ReadStartElement("Won");
				data[0,1] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,1] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadEndElement();
		
				win.ReadStartElement("MCX");
				win.ReadStartElement("Won");
				data[0,2] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,2] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("MCO");
				win.ReadStartElement("Won");
				data[0,3] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,3] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("TDX");
				win.ReadStartElement("Won");
				data[0,4] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,4] = int.Parse(win.ReadString());	
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("TDO");
				win.ReadStartElement("Won");
				data[0,5] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,5] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("UserX");
				win.ReadStartElement("Won");
				data[0,6] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,6] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("UserO");
				win.ReadStartElement("Won");
				data[0,7] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				data[1,7] = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadEndElement();

				win.Close();

			}
			catch(System.IO.FileNotFoundException err)
			{
				for(int i = 0; i < 2; i++)
				{
					for(int j = 0; j<8; j++)
					{
						data[i,j] = 0;
					}
				}
			}
			//update info
			switch(gameType)
			{
				case 1: //User as X
					if(this.menuItem20.Checked)
					{
						if(winner == 1)
						{
							data[1,1] = data[1,1] + 1;
							data[0,6] = data[0,6] + 1;
							data[1,6] = data[1,6] + 1;
						}
						else if(winner ==2)
						{
							data[1,6] = data[1,6] + 1;
							data[0,1] = data[0,1] + 1;
							data[1,1] = data[1,1] + 1;
						}
						else
						{
							data[1,6] = data[1,6] + 1;
							data[1,1] = data[1,1] + 1;
						}
					}
					else if(this.menuItem21.Checked)
					{
						if(winner == 1)
						{
							data[1,3] = data[1,3] + 1;
							data[0,6] = data[0,6] + 1;
							data[1,6] = data[1,6] + 1;
						}
						else if(winner ==2)
						{
							data[1,6] = data[1,6] + 1;
							data[0,3] = data[0,3] + 1;
							data[1,3] = data[1,3] + 1;
						}
						else
						{
							data[1,6] = data[1,6] + 1;
							data[1,3] = data[1,3] + 1;
						}
					}
					else
					{
						if(winner == 1)
						{
							data[1,5] = data[1,5] + 1;
							data[0,6] = data[0,6] + 1;
							data[1,6] = data[1,6] + 1;
						}
						else if(winner ==2)
						{
							data[1,6] = data[1,6] + 1;
							data[0,5] = data[0,5] + 1;
							data[1,5] = data[1,5] + 1;
						}
						else
						{
							data[1,6] = data[1,6] + 1;
							data[1,5] = data[1,5] + 1;
						}
					}
					break;
				case 2: //User as O
					if(this.menuItem20.Checked)
					{
						if(winner == 2)
						{
							data[1,0] = data[1,0] + 1;
							data[0,7] = data[0,7] + 1;
							data[1,7] = data[1,7] + 1;
						}
						else if(winner == 1)
						{
							data[1,7] = data[1,7] + 1;
							data[0,0] = data[0,0] + 1;
							data[1,0] = data[1,0] + 1;
						}
						else
						{
							data[1,7] = data[1,7] + 1;
							data[1,0] = data[1,0] + 1;
						}
					}
					else if(this.menuItem21.Checked)
					{
						if(winner == 2)
						{
							data[1,2] = data[1,2] + 1;
							data[0,7] = data[0,7] + 1;
							data[1,7] = data[1,7] + 1;
						}
						else if(winner == 1)
						{
							data[1,7] = data[1,7] + 1;
							data[0,2] = data[0,2] + 1;
							data[1,2] = data[1,2] + 1;
						}
						else
						{
							data[1,7] = data[1,7] + 1;
							data[1,2] = data[1,2] + 1;
						}
					}
					else
					{
						if(winner == 2)
						{
							data[1,4] = data[1,4] + 1;
							data[0,7] = data[0,7] + 1;
							data[1,7] = data[1,7] + 1;
						}
						else if(winner == 1)
						{
							data[1,7] = data[1,7] + 1;
							data[0,4] = data[0,4] + 1;
							data[1,4] = data[1,4] + 1;
						}
						else
						{
							data[1,7] = data[1,7] + 1;
							data[1,4] = data[1,4] + 1;
						}
					}
					break;
				case 3: //Two Users
					if(winner == 1)
					{
						data[1,7] = data[1,7] + 1;
						data[0,6] = data[0,6] + 1;
						data[1,6] = data[1,6] + 1;
					}
					else if(winner ==2)
					{
						data[1,6] = data[1,6] + 1;
						data[0,7] = data[0,7] + 1;
						data[1,7] = data[1,7] + 1;
					}
					else
					{
						data[1,6] = data[1,6] + 1;
						data[1,7] = data[1,7] + 1;
					}
					break;
				case 4: //Two Agents
					if(this.menuItem20.Checked)
					{
						if(winner == 1)
						{
							if(this.menuItem34.Checked)
							{
								data[1,3] = data[1,3] + 1;
							}
							else if(this.menuItem35.Checked)
							{
								data[1,5] = data[1,5] + 1;
							}
							else//(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
							}

							data[1,0] = data[1,0] + 1;
							data[0,0] = data[0,0] + 1;
						}
						else if(winner ==2)
						{
							data[1,0] = data[1,0] + 1;
							if(this.menuItem34.Checked)
							{
								data[0,3] = data[0,3] + 1;
								data[1,3] = data[1,3] + 1;
							}
							else if(this.menuItem35.Checked)
							{
								data[0,5] = data[0,5] + 1;
								data[1,5] = data[1,5] + 1;
							}
							else//(this.menuItem33.Checked)
							{
								data[0,1] = data[0,1] + 1;
								data[1,1] = data[1,1] + 1;
							}
						}
						else
						{
							if(this.menuItem34.Checked)
							{
								data[1,3] = data[1,3] + 1;
							}
							else if(this.menuItem35.Checked)
							{
								data[1,5] = data[1,5] + 1;
							}
							else//(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
							}
							
							data[1,0] = data[1,0] + 1;
						}
					}
					else if(this.menuItem21.Checked)
					{
						if(winner == 1)
						{
							if(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
							}
							else if(this.menuItem35.Checked)
							{
								data[1,5] = data[1,5] + 1;
							}
							else
							{
								data[1,3] = data[1,3] + 1;
							}
							data[0,2] = data[0,2] + 1;
							data[1,2] = data[1,2] + 1;
						}
						else if(winner ==2)
						{
							data[1,2] = data[1,2] + 1;
							if(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
								data[0,1] = data[0,1] + 1;
							}
							else if(this.menuItem35.Checked)
							{
								data[1,5] = data[1,5] + 1;
								data[0,5] = data[0,5] + 1;
							}
							else
							{
								data[0,3] = data[0,3] + 1;
								data[1,3] = data[1,3] + 1;
							}
						}
						else
						{
							data[1,2] = data[1,2] + 1;
							if(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
							}
							else if(this.menuItem35.Checked)
							{
								data[1,5] = data[1,5] + 1;
							}
							else
							{
								data[1,3] = data[1,3] + 1;
							}
						}
					}
					else //TDX vs TDO
					{
						if(winner == 1)
						{
							if(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
							}
							else if(this.menuItem34.Checked)
							{
								data[1,3] = data[1,3] + 1;
							}
							else
							{
								data[1,5] = data[1,5] + 1;
							}

							data[0,4] = data[0,4] + 1;
							data[1,4] = data[1,4] + 1;
						}
						else if(winner ==2)
						{
							data[1,4] = data[1,4] + 1;
							if(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
								data[0,1] = data[0,1] + 1;
							}
							else if(this.menuItem34.Checked)
							{
								data[1,3] = data[1,3] + 1;
								data[0,3] = data[0,3] + 1;
							}
							else
							{
								data[0,5] = data[0,5] + 1;
								data[1,5] = data[1,5] + 1;
							}

						}
						else
						{
							if(this.menuItem33.Checked)
							{
								data[1,1] = data[1,1] + 1;
							}
							else if(this.menuItem34.Checked)
							{
								data[1,3] = data[1,3] + 1;
							}
							else
							{
								data[1,5] = data[1,5] + 1;
							}
							data[1,4] = data[1,4] + 1;
						}
					}
					break;
			}


			//write info
			XmlTextWriter winXml = new XmlTextWriter("WinPercent.xml",System.Text.Encoding.Default);
			winXml.WriteStartDocument(true);
			winXml.WriteComment("This keeps track of the win percentage of each game played");
				
			winXml.WriteStartElement("Win");
				
			winXml.WriteStartElement("DPX");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,0].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,0].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();

			winXml.WriteStartElement("DPO");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,1].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,1].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();
		
			winXml.WriteStartElement("MCX");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,2].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,2].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();

			winXml.WriteStartElement("MCO");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,3].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,3].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();

			winXml.WriteStartElement("TDX");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,4].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,4].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();

			winXml.WriteStartElement("TDO");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,5].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,5].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();

			winXml.WriteStartElement("UserX");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,6].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,6].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();

			winXml.WriteStartElement("UserO");
			winXml.WriteStartElement("Won");
			winXml.WriteString(data[0,7].ToString());
			winXml.WriteEndElement();
			winXml.WriteStartElement("Played");
			winXml.WriteString(data[1,7].ToString());
			winXml.WriteEndElement();
			winXml.WriteEndElement();

			winXml.WriteEndElement();
			winXml.WriteEndDocument();
			
			winXml.Close();
		}

		//Configure Agent - Dynamic Programming
		private void menuItem12_Click(object sender, System.EventArgs e)
		{
			Form configDP = new Agent.ConfigureDP();
			configDP.Show();
		}

		//Configure Agent - Monte Carlo
		private void menuItem13_Click(object sender, System.EventArgs e)
		{
			Form configMC = new Agent.ConfigureMC();
			configMC.Show();
		}

		//Configure Agent - Temporal Difference
		private void menuItem14_Click(object sender, System.EventArgs e)
		{
			Form configTD = new Agent.ConfigureTD();
			configTD.Show();
		}

		private void menuItem17_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Developed by William Andrus\nAll Rights Reserved (c)2006\n\nDeveloped with Microsoft Framework 1.1.4322 SP1\nDeveloped in Microsoft Development Environment 2003 Version 7.1.3088","About Reinforcement Learning Tic-Tac-Toe",MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		//Monte Carlo will be used for the agent
		private void menuItem21_Click(object sender, System.EventArgs e)
		{

			this.menuItem21.Checked = true;
			this.menuItem20.Checked = false;
			this.menuItem22.Checked = false;
			this.menuItem34.Checked = true;
			this.menuItem33.Checked = false;
			this.menuItem35.Checked = false;
		}

		//Dynamic Programming will be used for the agent
		private void menuItem20_Click(object sender, System.EventArgs e)
		{

			this.menuItem21.Checked = false;
			this.menuItem20.Checked = true;
			this.menuItem22.Checked = false;
			this.menuItem34.Checked = false;
			this.menuItem33.Checked = true;
			this.menuItem35.Checked = false;
		}

		//Temporal Difference will be used for the agent
		private void menuItem22_Click(object sender, System.EventArgs e)
		{

			this.menuItem21.Checked = false;
			this.menuItem20.Checked = false;
			this.menuItem22.Checked = true;
			this.menuItem34.Checked = false;
			this.menuItem33.Checked = false;
			this.menuItem35.Checked = true;
		}

		private void menuItem16_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Tic-Tac-Toe (Also known as: Naught and Crosses)\n\n"+
							"Tic-Tac-Toe is a two-player game where X represents one player,\n"+
							"and O represents the other player. The game is played on a 3x3 grid,\n"+
							"and the first player to get 3 Xs or Os in a row wins. This can\n"+
							"be done horizontally, vertically, or diagonally. In the event of\n"+
							"no more moves are left and neither player wins is called a draw (AKA: Cat's Game).","How to play tic-tac-toe",MessageBoxButtons.OK,MessageBoxIcon.Information);
		}

		private void menuItem30_Click(object sender, System.EventArgs e)
		{
MessageBox.Show(@"
INTRODUCTION:
Artificial Intelligence (AI) has grown into a large area of research for developing concepts 
of creating human-like intelligence. One area of application is the development of programs 
to play games at a competitive level. One promising AI technique is Reinforcement Learning (RL). 
These concepts allow for a better understanding of AI itself by refining and applying these 
ideas to a wide spectrum of problem areas of varying difficulty while maintaining a simple 
understanding of what is being accomplished.

REINFORCEMENT LEARNING:
Reinforcement learning is the process for a program to interact within an environment and 
learn by receiving numerical rewards or punishment while trying to obtain a goal. In this case, 
the goal is to win when dealing with tic-tac-toe. The environment is �where� and �how� a program, 
that is using reinforcement learning, interacts against its opponents in the game. The reward/punishment 
is a scalar value. Depending on the program�s decisions and actions, it will make a move and recieve an
reward later.

DYNAMIC PROGRAMMING:
In dynamic programming the reward for each move is based upon all the possible values that could have
been taken. This concept is called a full backup, where each move is given a reward after is has been
taken.  

MONTE CARLO METHOD:
In monte carlo the reward for each of the moves made in the game is based upon the ending result, where
each of the moves would be given a reward, when the game has finished.

TEMPORAL DIFFERENCE:
In temporal difference the reward is given after each move (like Dynamic Programming), but the reward is
based on the move that was made (like monte carlo method).","Small Introduction to Reinforcement Learning",MessageBoxButtons.OK,MessageBoxIcon.Information);
		}

		private void pictureBox1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void menuItem31_Click(object sender, System.EventArgs e)
		{
			TicTacToe.ComputerVsComputerCount countForm = new ComputerVsComputerCount();
			countForm.Show();
		}

		private void menuItem29_Click(object sender, System.EventArgs e)
		{
			Graph graph = new Graph();
			graph.Show();
		}

		//DP XML File open
		private void menuItem25_Click(object sender, System.EventArgs e)
		{
			OpenXML file = new OpenXML("DPstates.xml");
			file.Show();
		}

		//Open MC XML file
		private void menuItem26_Click(object sender, System.EventArgs e)
		{
			OpenXML file = new OpenXML("MCstates.xml");
			file.Show();

		}

		private void menuItem36_Click(object sender, System.EventArgs e)
		{

		}

		private void menuItem28_Click(object sender, System.EventArgs e)
		{
			StateSearch statesearch = new StateSearch();
			statesearch.Show();
		}

		private void menuItem27_Click(object sender, System.EventArgs e)
		{
			OpenXML file = new OpenXML("TDstates.xml");
			file.Show();
		}

		private void menuItem32_Click(object sender, System.EventArgs e)
		{
			int DPcount = 0;
			int MCcount = 0;
			int TDcount = 0;
			//int MAX_COUNT = 6620; //possible tictactoe states given this games layout

			//Count DP states
			try
			{
				XmlTextReader DPstates = new XmlTextReader("DPstates.xml");
				DPstates.ReadStartElement("DynamicProgramming");
				while(DPstates.Read())
				{
					if(DPstates.HasAttributes)
					{
						DPcount++;
						DPstates.ReadString();
					}
				}
				DPstates.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}

			//Count MC states
			try
			{
				XmlTextReader MCstates = new XmlTextReader("MCstates.xml");
				MCstates.ReadStartElement("MonteCarlo");
				while(MCstates.Read())
				{
					if(MCstates.HasAttributes)
					{
						MCcount++;
						MCstates.ReadString();
					}
				}
				MCstates.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}

			//Count TD states
			try
			{
				XmlTextReader TDstates = new XmlTextReader("TDstates.xml");
				TDstates.ReadStartElement("TemporalDifference");
				while(TDstates.Read())
				{
					if(TDstates.HasAttributes)
					{
						TDstates.ReadString();
						TDcount++;
					}
				}
				TDstates.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}
			

			MessageBox.Show("Dynamic Programming: "+DPcount+" out of 765\n"+
							"Monte Carlo: "+MCcount+" out of 765\n"+
							"Temporal Difference: "+TDcount+" out of 765\n","How many states found so far?",MessageBoxButtons.OK,MessageBoxIcon.Information);
		}

		private void menuItem33_Click(object sender, System.EventArgs e)
		{
			this.menuItem33.Checked = true;
			this.menuItem34.Checked = false;
			this.menuItem35.Checked = false;
			menuItem8_Click(sender,e);
		}

		private void menuItem34_Click(object sender, System.EventArgs e)
		{
			this.menuItem33.Checked = false;
			this.menuItem34.Checked = true;
			this.menuItem35.Checked = false;
			menuItem8_Click(sender,e);
		}

		private void menuItem35_Click(object sender, System.EventArgs e)
		{
			this.menuItem33.Checked = false;
			this.menuItem34.Checked = false;
			this.menuItem35.Checked = true;
			menuItem8_Click(sender,e);
		}	
	}
}
