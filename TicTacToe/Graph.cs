using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

namespace TicTacToe
{
	/// <summary>
	/// Summary description for Graph.
	/// </summary>
	public class Graph : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox pictureBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private double [] data = new double[8];

		public Graph()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//TODO:     READ IN DATA
			//TestData
			//data = new float[] {30.0F,40.0F,20.0F,50.0F,100.0F,1.0F,60.0F,55.0F};
			int winNum;
			int played;
			double ans;

			try
			{
				XmlTextReader win = new XmlTextReader("WinPercent.xml");
				
				win.ReadStartElement("Win");
				
				win.ReadStartElement("DPX");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[0] = ans;
				}
				else
				{
					data[0] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("DPO");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[1] = ans;
				}
				else
				{
					data[1] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();
		
				win.ReadStartElement("MCX");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[2] = ans;
				}
				else
				{
					data[2] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("MCO");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[3] = ans;
				}
				else
				{
					data[3] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("TDX");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[4] = ans;
				}
				else
				{
					data[4] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("TDO");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[5] = ans;
				}
				else
				{
					data[5] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("UserX");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[6] = ans;
				}
				else
				{
					data[6] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();

				win.ReadStartElement("UserO");
				win.ReadStartElement("Won");
				winNum = int.Parse(win.ReadString());
				win.ReadEndElement();
				win.ReadStartElement("Played");
				played = int.Parse(win.ReadString());
				if(played != 0)
				{
					ans = ((winNum * 100) / played);
					data[7] = ans;
				}
				else
				{
					data[7] = 0.0F;
				}
				win.ReadEndElement();
				win.ReadEndElement();
				win.Close();

			}
			catch(System.IO.FileNotFoundException err)
			{
				for(int j = 0; j<8; j++)
				{
					data[j] = 0.0F;
				}
			}

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Graph));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.GhostWhite;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(776, 272);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintGraph);
			// 
			// Graph
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(774, 267);
			this.Controls.Add(this.pictureBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "Graph";
			this.Text = "Graph";
			this.ResumeLayout(false);

		}
		#endregion

		private void PaintGraph(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Graphics g = e.Graphics;
			Brush redBrush = new SolidBrush(Color.Red);
			Brush orangeBrush = new SolidBrush(Color.Orange);
			Brush yellowBrush = new SolidBrush(Color.Yellow);
			Brush greenBrush = new SolidBrush(Color.Green);
			Brush blueBrush = new SolidBrush(Color.Blue);
			Brush indigoBrush = new SolidBrush(Color.Indigo);
			Brush violetBrush = new SolidBrush(Color.Violet);
			Brush purpleBrush = new SolidBrush(Color.Purple);
			Brush blackBrush = new SolidBrush(Color.Black);
			Font font = new Font("Trebuchet MS",10);
			Font smallFont = new Font("Trebuchet MS",8);
			
			float width = this.pictureBox1.Width/8;

			float DPXheight = (float)data[0];
			float DPXX = 0.0F;
			float DPXY = this.pictureBox1.Height - DPXheight - 20;

			g.FillRectangle(redBrush,DPXX,DPXY,width,DPXheight);
			g.DrawString(data[0].ToString()+"%",font,blackBrush,DPXX+5,DPXY - 20);
			g.DrawString("DP Agent X",smallFont,blackBrush,DPXX,this.pictureBox1.Height - 20);

			float DPOheight = (float)data[1];
			float DPOX = DPXX + width;
			float DPOY = this.pictureBox1.Height - DPOheight - 20;

			g.FillRectangle(orangeBrush,DPOX,DPOY,width,DPOheight);
			g.DrawString(data[1].ToString()+"%",font,blackBrush,DPOX+5,DPOY - 20);
			g.DrawString("DP Agent O",smallFont,blackBrush,DPOX,this.pictureBox1.Height - 20);

			float MCXheight = (float)data[2];
			float MCXX = DPOX + width;
			float MCXY = this.pictureBox1.Height - MCXheight - 20;

			g.FillRectangle(yellowBrush,MCXX,MCXY,width,MCXheight);
			g.DrawString(data[2].ToString()+"%",font,blackBrush,MCXX+5,MCXY - 20);
			g.DrawString("MC Agent X",smallFont,blackBrush,MCXX,this.pictureBox1.Height - 20);

			float MCOheight = (float)data[3];
			float MCOX = MCXX + width;
			float MCOY = this.pictureBox1.Height - MCOheight - 20;

			g.FillRectangle(greenBrush,MCOX,MCOY,width,MCOheight);
			g.DrawString(data[3].ToString()+"%",font,blackBrush,MCOX+5,MCOY - 20);
			g.DrawString("MC Agent O",smallFont,blackBrush,MCOX,this.pictureBox1.Height - 20);

			float TDXheight = (float)data[4];
			float TDXX = MCOX + width;
			float TDXY = this.pictureBox1.Height - TDXheight - 20;

			g.FillRectangle(blueBrush,TDXX,TDXY,width,TDXheight);
			g.DrawString(data[4].ToString()+"%",font,blackBrush,TDXX+5,TDXY - 20);
			g.DrawString("TD Agent X",smallFont,blackBrush,TDXX,this.pictureBox1.Height - 20);


			float TDOheight = (float)data[5];
			float TDOX = TDXX + width;
			float TDOY = this.pictureBox1.Height - TDOheight - 20;

			g.FillRectangle(indigoBrush,TDOX,TDOY,width,TDOheight);
			g.DrawString(data[5].ToString()+"%",font,blackBrush,TDOX+5,TDOY - 20);
			g.DrawString("TD Agent O",smallFont,blackBrush,TDOX,this.pictureBox1.Height - 20);

			float userXheight = (float)data[6];
			float userXX = TDOX + width;
			float userXY = this.pictureBox1.Height - userXheight - 20;

			g.FillRectangle(violetBrush,userXX,userXY,width,userXheight);
			g.DrawString(data[6].ToString()+"%",font,blackBrush,userXX+5,userXY - 20);
			g.DrawString("User X",smallFont,blackBrush,userXX,this.pictureBox1.Height - 20);


			float userOheight = (float)data[7];
			float userOX = userXX + width;
			float userOY = this.pictureBox1.Height - userOheight - 20;

			g.FillRectangle(purpleBrush,userOX,userOY,width,userOheight);
			g.DrawString(data[7].ToString()+"%",font,blackBrush,userOX+5,userOY - 20);
			g.DrawString("User O",smallFont,blackBrush,userOX,this.pictureBox1.Height - 20);


			//Destroy brushes
			font.Dispose();
			smallFont.Dispose();
			redBrush.Dispose();
			orangeBrush.Dispose();
			yellowBrush.Dispose();
			greenBrush.Dispose();
			blueBrush.Dispose();
			indigoBrush.Dispose();
			blackBrush.Dispose();
			violetBrush.Dispose();
			purpleBrush.Dispose();
		}
	}
}
