using System;
using System.Xml;

namespace TicTacToe.Agent
{
	public class MonteCarlo
	{
		private double Epsilon = 0.1;
		private double StepSize = 0.1;
		private int winReward = 1;
		private int tieReward = 0;
		private int lostReward = -1;
		private bool autoExplore = true;
		private int outOfBoundsReward;
		private const int MAX_STATES = 5; 
		private double largestValue = -2.0;
		private int move;//represents the decision of where to move
		private int moves = 0;//represents the number of moves made;
		//Keep track for deep backup
		private string[] visitedStates = new string[MAX_STATES];
		/*
		 * Changed from 7000 to 1000 
		 * for the optimized version 1.1 on 3/6/06
		 * 
		 */
		//Read/Write states in the array/file
		private const int MAX = 800; 
		//[state,value] x MAX
		private string [,] states = new string[2,MAX];
		private int player;
		private double defaultValue = 2.0;

		public MonteCarlo(int player)
		{
			this.player = player;
			/*
			 * Read in the configuration
			 * 
			 */ 
			try
			{
				XmlTextReader MCconfig = new XmlTextReader("MCConfig.xml");
				
				MCconfig.ReadStartElement("MonteCarlo");
				MCconfig.ReadStartElement("Epsilon");
				Epsilon = double.Parse(MCconfig.ReadString());
				MCconfig.ReadEndElement();
				MCconfig.ReadStartElement("Stepsize");
				StepSize = double.Parse(MCconfig.ReadString());
				MCconfig.ReadEndElement();
				MCconfig.ReadStartElement("Reward");
				MCconfig.ReadStartElement("Win");
				winReward = int.Parse(MCconfig.ReadString());
				MCconfig.ReadEndElement();
				MCconfig.ReadStartElement("Tie");
				tieReward = int.Parse(MCconfig.ReadString());
				MCconfig.ReadEndElement();
				MCconfig.ReadStartElement("Loss");
				lostReward = int.Parse(MCconfig.ReadString());
				MCconfig.ReadEndElement();
				MCconfig.ReadEndElement();
				MCconfig.ReadStartElement("AutoExplore");
				string ans = MCconfig.ReadString();
				if(ans == "Yes")
				{
					autoExplore = true;
				}
				else //ans == no
				{
					autoExplore = false;
				}
				MCconfig.Close();

			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}

			outOfBoundsReward = winReward + 1;

			/*
			 * Read in the seen states
			 * 
			 * <?xml>
			 * <MonteCarlo>
			 * <state string="?????????">value</state>
			 * </MonteCarlo>
			 * </xml>
			 */ 
			try
			{
				XmlTextReader MCstates = new XmlTextReader("MCstates.xml");
				MCstates.ReadStartElement("MonteCarlo");
				int i = 0;
				this.states[0,i] = MCstates.GetAttribute("string");
				this.states[1,i] = MCstates.ReadString();
				i++;
				while(MCstates.Read())
				{
					this.states[0,i] = MCstates.GetAttribute("string");
					this.states[1,i] = MCstates.ReadString();
					i++;
				}
				MCstates.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}
		}

		//MonteCarlo updates at the end of each game

		public void update(int result, int gameType)
		{
			if(gameType == 4)
			{
				//Read in values, incase the other agent has updated it;
				try
				{
					XmlTextReader MCstatesRead = new XmlTextReader("MCstates.xml");
					MCstatesRead.ReadStartElement("MonteCarlo");
					int i = 0;
					this.states[0,i] = MCstatesRead.GetAttribute("string");
					this.states[1,i] = MCstatesRead.ReadString();
					i++;
					while(MCstatesRead.Read())
					{
						this.states[0,i] = MCstatesRead.GetAttribute("string");
						this.states[1,i] = MCstatesRead.ReadString();
						i++;
					}
					MCstatesRead.Close();
				}
				catch(System.IO.FileNotFoundException err)
				{
					//do nothing, use defaults
				}
				catch(Exception err)
				{
					System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
				}
			}

			bool found;
			//update values
			//for each of the moves
			for(int i = 0; i< MAX_STATES; i++)
			{
				found = false;
				//search the list to see if the state is there
				for(int j = 0; j< MAX; j++)
				{

					/*
					 * Changed for version 1.1 on 3/3/06
					 * 
					 */
					int rotationalFound = -1;
					string visitedState1 = "";
					string visitedState2 = "";
					string visitedState3 = "";
					string visitedState4 = "";
					string visitedState5 = "";
					string visitedState6 = "";
					string visitedState7 = "";
					if(this.visitedStates[i] != null)
					{
						visitedState1 = (this.visitedStates[i][2].ToString() + this.visitedStates[i][5].ToString() + this.visitedStates[i][8].ToString() + this.visitedStates[i][1].ToString() + this.visitedStates[i][4].ToString() + this.visitedStates[i][7].ToString() + this.visitedStates[i][0].ToString() + this.visitedStates[i][3].ToString() + this.visitedStates[i][6].ToString());
						visitedState2 = (this.visitedStates[i][8].ToString() + this.visitedStates[i][7].ToString() + this.visitedStates[i][6].ToString() + this.visitedStates[i][5].ToString() + this.visitedStates[i][4].ToString() + this.visitedStates[i][3].ToString() + this.visitedStates[i][2].ToString() + this.visitedStates[i][1].ToString() + this.visitedStates[i][0].ToString());
						visitedState3 = (this.visitedStates[i][6].ToString() + this.visitedStates[i][3].ToString() + this.visitedStates[i][0].ToString() + this.visitedStates[i][7].ToString() + this.visitedStates[i][4].ToString() + this.visitedStates[i][1].ToString() + this.visitedStates[i][8].ToString() + this.visitedStates[i][5].ToString() + this.visitedStates[i][2].ToString());
						
						visitedState4 = (this.visitedStates[i][6].ToString() + this.visitedStates[i][7].ToString() + this.visitedStates[i][8].ToString() + this.visitedStates[i][3].ToString() + this.visitedStates[i][4].ToString() + this.visitedStates[i][5].ToString() + this.visitedStates[i][0].ToString() + this.visitedStates[i][1].ToString() + this.visitedStates[i][2].ToString());
						visitedState5 = (this.visitedStates[i][2].ToString() + this.visitedStates[i][1].ToString() + this.visitedStates[i][0].ToString() + this.visitedStates[i][5].ToString() + this.visitedStates[i][4].ToString() + this.visitedStates[i][3].ToString() + this.visitedStates[i][8].ToString() + this.visitedStates[i][7].ToString() + this.visitedStates[i][6].ToString());
						visitedState6 = (this.visitedStates[i][8].ToString() + this.visitedStates[i][5].ToString() + this.visitedStates[i][2].ToString() + this.visitedStates[i][7].ToString() + this.visitedStates[i][4].ToString() + this.visitedStates[i][1].ToString() + this.visitedStates[i][6].ToString() + this.visitedStates[i][3].ToString() + this.visitedStates[i][0].ToString());
						visitedState7 = (this.visitedStates[i][0].ToString() + this.visitedStates[i][3].ToString() + this.visitedStates[i][6].ToString() + this.visitedStates[i][1].ToString() + this.visitedStates[i][4].ToString() + this.visitedStates[i][7].ToString() + this.visitedStates[i][2].ToString() + this.visitedStates[i][5].ToString() + this.visitedStates[i][8].ToString());				
					
					}
					if(this.visitedStates[i] == this.states[0,j])
					{
						rotationalFound = 0;
					}
					else if(visitedState1 == this.states[0,j])
					{
						rotationalFound = 1;
					}
					else if(visitedState2 == this.states[0,j])
					{
						rotationalFound = 2;
					}
					else if(visitedState3 == this.states[0,j])
					{
						rotationalFound = 3;
					}
					else if(visitedState4 == this.states[0,j])
					{
						rotationalFound = 4;
					}
					else if(visitedState5 == this.states[0,j])
					{
						rotationalFound = 5;
					}
					else if(visitedState6 == this.states[0,j])
					{
						rotationalFound = 6;
					}
					else if(visitedState7 == this.states[0,j])
					{
						rotationalFound = 7;
					}
					
					/*
					 * Changed if statement for version 1.1 on 3/3/06
					 */
					if(rotationalFound >= 0 && this.visitedStates[i] != null)
					{
						found = true;
						//newValue = oldValue + stepSize * ( REWARD - oldValue);
						//check for winning state for more optimize update
						if(result == 1)
						{
							try
							{
								if(this.visitedStates[i+1] == null)//changed to == might have been an error to have !=
								{
									this.states[1,j] = winReward.ToString();
								}
								else
								{
									this.states[1,j] = Convert.ToString(double.Parse(states[1,j]) + StepSize * (winReward - double.Parse(states[1,j])));
								}
							}
							catch(System.IndexOutOfRangeException err)
							{
								this.states[1,j] = Convert.ToString(double.Parse(states[1,j]) + StepSize * (winReward - double.Parse(states[1,j])));
							}
						}
						else if(result == 0)
						{
							try
							{
								if(i == MAX_STATES || this.visitedStates[i+1] == null )
								{
									this.states[1,j] = tieReward.ToString();
								}
								else
								{
									this.states[1,j] = Convert.ToString(double.Parse(states[1,j]) + StepSize * (tieReward - double.Parse(states[1,j])));
								}
							}
							catch(System.IndexOutOfRangeException err)
							{
								this.states[1,j] = Convert.ToString(double.Parse(states[1,j]) + StepSize * (winReward - double.Parse(states[1,j])));
							}
						}
						else //result == -1
						{
							try
							{
								if(i == MAX_STATES || this.visitedStates[i+1] == null)
								{
									this.states[1,j] = lostReward.ToString();
								}
								else
								{
									this.states[1,j] = Convert.ToString(double.Parse(states[1,j]) + StepSize * (lostReward - double.Parse(states[1,j])));
								}
							}
							catch(System.IndexOutOfRangeException err)
							{
								this.states[1,j] = Convert.ToString(double.Parse(states[1,j]) + StepSize * (winReward - double.Parse(states[1,j])));
							}
						}
						j = MAX; //Exit loop
					}

				}
				//A new state is found
				if(found == false)
				{
					int location = 0;
					while(states[0,location] != null)
					{
						location++;
					}
					if(result == 1)
					{
						this.states[0,location] = this.visitedStates[i];
						this.states[1,location] = Convert.ToString(this.defaultValue + StepSize * (winReward - this.defaultValue));
					}
					else if(result == 0)
					{
						this.states[0,location] = this.visitedStates[i];
						this.states[1,location] = Convert.ToString(this.defaultValue + StepSize * (tieReward - this.defaultValue));
					}
					else //result == -1
					{
						this.states[0,location] = this.visitedStates[i];
						this.states[1,location] = Convert.ToString(this.defaultValue + StepSize * (lostReward - this.defaultValue));
					}
				}
			}



			

			//write to xml
			XmlTextWriter MCstates = new XmlTextWriter("MCstates.xml",System.Text.Encoding.Default);
			MCstates.WriteStartDocument(true);
			MCstates.WriteStartElement("MonteCarlo");
			int inc = 0;
			while(inc< MAX && states[0,inc] != null)
			{
				MCstates.WriteStartElement("state");
				MCstates.WriteAttributeString("string",states[0,inc]);
				MCstates.WriteString(states[1,inc]);
				MCstates.WriteEndElement();
				inc++;
			}
			MCstates.WriteEndElement();
			MCstates.WriteEndDocument();
			MCstates.Close();
		}

		//find the best move and return it's answer.
		public int getMove(int[] board, int BOARD_SIZE)
		{
			double [] values = new double[9];
			for(int i = 0; i < BOARD_SIZE; i++)
			{
				values[i] = -100;
			}
			//Is autoExplore on?
			if(this.autoExplore)
			{
				defaultValue = this.outOfBoundsReward;
			}
			else
			{
				//Set the default value to the average of the rewards
				// Ex: 1+0+(-1) = 0/3=0  
				//     0+(-1)+(-2) = -3/3=-1   
				//     1+(-1)+(-1) = -1/3 = -.333
				defaultValue = (this.winReward + this.tieReward + this.lostReward)/3;
			}

			//Find Best Move by Value
			this.largestValue = -2;
			/*
			 * Changed for optimization 
			 * for version 1.1 on 3/3/06
			 * 
			 * Instead of string state;
			 * replaced it with
			 * 
			 * string state1;
			 * string state2;
			 * string state3;
			 * string state4;...
			 * 
			 * to represent the four rotations of the tic-tac-toe board
			 */
			string state1;
			string state2;
			string state3;
			string state4;
			
			string state5;
			string state6;
			string state7;
			string state8;

			//for each position on the board loop through
			for(int i = 0; i<BOARD_SIZE; i++)
			{
				/*
				 * Changed on 3/3/06 
				 * for optimization for version 1.1
				 * (see above for changes made)
				 * 
				 */
				state1 = "";
				state2 = "";
				state3 = "";
				state4 = "";
				state5 = "";
				state6 = "";
				state7 = "";
				state8 = "";

				//If current board position is empty
				if(board[i] == 0)
				{
					//If the current position is not the position currently viewing 
					//possible changes for then leave as is, else subsitute the
					//current players value.
					for(int k = 0; k<BOARD_SIZE; k++)
					{
						if(i != k)
						{
							/*
							 * Version 1.1 Change
							 */
							//Leave default as first
							state1 = state1.Insert(state1.Length,board[k].ToString());
						}
						else
						{
							/*
							 * Version 1.1 Change
							 */
							state1 = state1.Insert(state1.Length,this.player.ToString());
						}
					}
					
					//Version 1.1
					//Make the state2,state3,state4		
					state2 = (state1[2].ToString()+ state1[5].ToString() + state1[8].ToString() + state1[1].ToString() + state1[4].ToString() + state1[7].ToString() + state1[0].ToString() + state1[3].ToString() + state1[6].ToString());
					state3 = (state1[8].ToString()+ state1[7].ToString() + state1[6].ToString() + state1[5].ToString() + state1[4].ToString() + state1[3].ToString() + state1[2].ToString() + state1[1].ToString() + state1[0].ToString());
					state4 = (state1[6].ToString()+ state1[3].ToString() + state1[0].ToString() + state1[7].ToString() + state1[4].ToString() + state1[1].ToString() + state1[8].ToString() + state1[5].ToString() + state1[2].ToString());


					state5 = (state1[6].ToString()+ state1[7].ToString() + state1[8].ToString() + state1[3].ToString() + state1[4].ToString() + state1[5].ToString() + state1[0].ToString() + state1[1].ToString() + state1[2].ToString());
					state6 = (state1[2].ToString()+ state1[1].ToString() + state1[0].ToString() + state1[5].ToString() + state1[4].ToString() + state1[3].ToString() + state1[8].ToString() + state1[7].ToString() + state1[6].ToString());
					state7 = (state1[8].ToString()+ state1[5].ToString() + state1[2].ToString() + state1[7].ToString() + state1[4].ToString() + state1[1].ToString() + state1[6].ToString() + state1[3].ToString() + state1[0].ToString());
					state8 = (state1[0].ToString()+ state1[3].ToString() + state1[6].ToString() + state1[1].ToString() + state1[4].ToString() + state1[7].ToString() + state1[2].ToString() + state1[5].ToString() + state1[8].ToString());


					//find
					bool found = false;
					for(int j = 0; j< MAX; j++)
					{
						if(state1 == this.states[0,j])
						{
							found = true;
							//get the value
							values[i] = double.Parse(this.states[1,j]);
							if(values[i] > largestValue && board[i]==0)
							{
								largestValue = values[i];
								this.move = i;
							}
						}
						else if(state2 == this.states[0,j])
						{
							found = true;
							//get the value
							values[i] = double.Parse(this.states[1,j]);
							if(values[i] > largestValue && board[i]==0)
							{
								largestValue = values[i];
								this.move = i;
							}
						}
						else if(state3 == this.states[0,j])
						{
							found = true;
							//get the value
							values[i] = double.Parse(this.states[1,j]);
							if(values[i] > largestValue && board[i]==0)
							{
								largestValue = values[i];
								this.move = i;
							}
						}
						else if(state4 == this.states[0,j])
						{
							found = true;
							//get the value
							values[i] = double.Parse(this.states[1,j]);
							if(values[i] > largestValue && board[i]==0)
							{
								largestValue = values[i];
								this.move = i;
							}
						}
					}
					if(found == false)
					{
						if(defaultValue > largestValue)
						{
							largestValue = defaultValue;
							this.move = i;
						}
						values[i] = defaultValue;
					}
				}

			}

			//Check Epsilon Range
			double range = largestValue - this.Epsilon;
			Random randInt = new Random();
			//Find values within range and randomly select among them
			for(int i = 0; i < BOARD_SIZE; i++)
			{
				if(values[i] >= range && move != i && board[i]==0)
				{
					//Randomly select between the two
					if(Math.Round(randInt.NextDouble(),0) == 1)  //then change to new
					{
						//set move
						move = i;
					}
				}
			}	


			/*
			 * Optimized update for version 1.1
			 * 
			 */
			state1 = "";
			state2 = "";
			state3 = "";
			state4 = "";
			state5 = "";
			state6 = "";
			state7 = "";
			state8 = "";


			//Debugging/////////////////////////////////111111111111111111111111111111111
			if(moves>5)
			{
				System.Windows.Forms.MessageBox.Show("AHHHH");
			}
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			this.visitedStates[moves] = "";
			int p;
			p = 0;
			try
			{
				for(; p< BOARD_SIZE; p++)
				{
					if(p == move && board[p]==0)
					{
						this.visitedStates[moves] = this.visitedStates[moves].Insert(this.visitedStates[moves].Length,this.player.ToString());
					}
					else
					{
						this.visitedStates[moves] = this.visitedStates[moves].Insert(this.visitedStates[moves].Length,board[p].ToString());
					}
				}
			}
			catch(Exception e)
			{
				System.Windows.Forms.MessageBox.Show(e+"\n"+"BOARD_SIZE "+BOARD_SIZE+"\np "+p+"\nmoves "+moves);
			}

			moves++;
			return move;
		}
	}
	
	public class DynamicProgramming
	{
		private double Epsilon = 0.1;
		private double StepSize = 0.1;
		private int winReward = 1;
		private int tieReward = 0;
		private int lostReward = -1;
		private bool autoExplore = true;
		private int outOfBoundsReward;
		private const int MAX_STATES = 5; 
		private double largestValue = -2.0;
		private int move;//represents the decision of where to move
		private int moves = 0;//represents the number of moves made;
		//Read/Write states in the array/file
		/*
		 * Version 1.1 Changed Max from 7000 to 1000 on 3/3/06
		 * 
		 */
		private const int MAX = 800;
		private string [,] states = new string[2,MAX];
		private int player;
		private double defaultValue = 2.0;
		private string previousState = "";

		public DynamicProgramming(int player)
		{
			this.player = player;
			/*
			 * Read in the configuration
			 * 
			 */ 
			try
			{
				XmlTextReader DPconfig = new XmlTextReader("DPConfig.xml");
				
				DPconfig.ReadStartElement("DynamicProgramming");
				DPconfig.ReadStartElement("Epsilon");
				Epsilon = double.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Stepsize");
				StepSize = double.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Reward");
				DPconfig.ReadStartElement("Win");
				winReward = int.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Tie");
				tieReward = int.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("Loss");
				lostReward = int.Parse(DPconfig.ReadString());
				DPconfig.ReadEndElement();
				DPconfig.ReadEndElement();
				DPconfig.ReadStartElement("AutoExplore");
				string ans = DPconfig.ReadString();
				if(ans == "Yes")
				{
					autoExplore = true;
				}
				else //ans == no
				{
					autoExplore = false;
				}
				DPconfig.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}

			outOfBoundsReward = winReward + 1;
			/*
			 * Read in the seen states
			 * 
			 * <?xml>
			 * <DynamicProgramming>
			 * <state string="?????????">value</state>
			 * </MonteCarlo>
			 * </xml>
			 */ 
			try
			{
				XmlTextReader DPstates = new XmlTextReader("DPstates.xml");
				DPstates.ReadStartElement("DynamicProgramming");
				int i = 0;
				this.states[0,i] = DPstates.GetAttribute("string");
				this.states[1,i] = DPstates.ReadString();
				i++;
				while(DPstates.Read())
				{
					this.states[0,i] = DPstates.GetAttribute("string");
					this.states[1,i] = DPstates.ReadString();
					i++;
				}
				DPstates.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}
		}
		//updates the xml file during the game
		public void innerUpdate(double newValue, string currentState)
		{
			bool found = false;
			for(int j = 0; j< MAX; j++)
			{
				/*
				 * addition for optimization for version 1.1 on 3/3/06
				 * 
				 */
				string currentState2 = (currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString());
				string currentState3 = (currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString());
				string currentState4 = (currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString());
				string currentState5 = (currentState[6].ToString() + currentState[7].ToString() + currentState[8].ToString() + currentState[3].ToString() + currentState[4].ToString() + currentState[5].ToString() + currentState[0].ToString() + currentState[1].ToString() + currentState[2].ToString());
				string currentState6 = (currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString());
				string currentState7 = (currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString());
				string currentState8 = (currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString());


				if(currentState == this.states[0,j] || currentState2 == this.states[0,j]  || currentState3 == this.states[0,j] || currentState4 == this.states[0,j] || currentState5 == this.states[0,j]|| currentState6 == this.states[0,j]|| currentState7 == this.states[0,j]|| currentState8 == this.states[0,j])
				{
					found = true;
					this.states[1,j] = newValue.ToString();
					j = MAX; //Exit loop
				}

			}
			//A new state is found
			if(found == false)
			{
				int location = 0;
				//traverse through
				while(states[0,location] != null)
				{
					location++;
				}
				this.states[0,location] = currentState;
				this.states[1,location] = newValue.ToString();
			}

			//write to xml
			XmlTextWriter DPstates = new XmlTextWriter("DPstates.xml",System.Text.Encoding.Default);
			DPstates.WriteStartDocument(true);
			DPstates.WriteStartElement("DynamicProgramming");
			int inc = 0;
			while(inc< MAX && states[0,inc] != null)
			{
				DPstates.WriteStartElement("state");
				DPstates.WriteAttributeString("string",states[0,inc]);
				DPstates.WriteString(states[1,inc]);
				DPstates.WriteEndElement();
				inc++;
			}
			DPstates.WriteEndElement();
			DPstates.WriteEndDocument();
			DPstates.Close();
		}

		//find the best move and return it's answer.
		public int getMove(int[] board, int BOARD_SIZE)
		{
			string currentState = "";
			double newValue = 0.0;
			double [] values = new double[9];

			//initialize the values to an impossible low number
			for(int i = 0; i < BOARD_SIZE; i++)
			{
				values[i] = -100;
				currentState = currentState.Insert(currentState.Length,board[i].ToString());
			}

			//Is autoExplore on?
			if(this.autoExplore)
			{
				defaultValue = this.outOfBoundsReward;
			}
			else
			{
				// 1+0+(-1) = 0/3=0   0+(-1)+(-2) = -3/3=-1   1+(-1)+(-1) = -1/3 = -.333
				defaultValue = (this.winReward + this.tieReward + this.lostReward)/3;
			}
			

			//Section: Find Best Move by Value
			this.largestValue = -2;
			string state;
			for(int i = 0; i<BOARD_SIZE; i++)
			{
				state = "";
				if(board[i] == 0)
				{
					//make string value of a possible state
					for(int k = 0; k<BOARD_SIZE; k++)
					{
						if(i != k)
						{
							state = state.Insert(state.Length,board[k].ToString());
						}
						else
						{
							state = state.Insert(state.Length,this.player.ToString());
						}
					}

					/*
					 * Optimize update for version 1.1 on 3/3/06
					 */
					string state2 = (state[2].ToString() + state[5].ToString() + state[8].ToString() + state[1].ToString() + state[4].ToString() + state[7].ToString() + state[0].ToString() + state[3].ToString() + state[6].ToString());
					string state3  = (state[8].ToString() + state[7].ToString() + state[6].ToString() + state[5].ToString() + state[4].ToString() + state[3].ToString() + state[2].ToString() + state[1].ToString() + state[0].ToString());
					string state4 = (state[6].ToString() + state[3].ToString() + state[0].ToString() + state[7].ToString() + state[4].ToString() + state[1].ToString() + state[8].ToString() + state[5].ToString() + state[2].ToString());


					string state5 = (state[6].ToString() + state[7].ToString() + state[8].ToString() + state[3].ToString() + state[4].ToString() + state[5].ToString() + state[0].ToString() + state[1].ToString() + state[2].ToString());
					string state6 = (state[2].ToString() + state[1].ToString() + state[0].ToString() + state[5].ToString() + state[4].ToString() + state[3].ToString() + state[8].ToString() + state[7].ToString() + state[6].ToString());
					string state7 = (state[8].ToString() + state[5].ToString() + state[2].ToString() + state[7].ToString() + state[4].ToString() + state[1].ToString() + state[6].ToString() + state[3].ToString() + state[0].ToString());
					string state8 = (state[0].ToString() + state[3].ToString() + state[6].ToString() + state[1].ToString() + state[4].ToString() + state[7].ToString() + state[2].ToString() + state[5].ToString() + state[8].ToString());

					//find that state
					bool found = false;
					for(int j = 0; j< MAX; j++)
					{
						/*
						 * Changed for version 1.1 on 3/3/06
						 */
						if(state == this.states[0,j] || state2 == this.states[0,j] || state3 == this.states[0,j] || state4 == this.states[0,j] || state5 == this.states[0,j]|| state6 == this.states[0,j]|| state7 == this.states[0,j]|| state8 == this.states[0,j])
						{
							found = true;
							values[i] = double.Parse(this.states[1,j]);
							if(values[i] > largestValue && board[i]==0)
							{
								largestValue = values[i];
								this.move = i;
							}
						}
					}
					if(found == false)
					{
						if(defaultValue > largestValue)
						{
							largestValue = defaultValue;
							this.move = i;
						}
						values[i] = defaultValue;
					}
				}
			}

			//update newValue;


			//Check Epsilon Range
			double range = largestValue - this.Epsilon;
			Random randInt = new Random();
			//Find values within range and randomly select among them
			for(int i = 0; i < BOARD_SIZE; i++)
			{
				if(values[i] >= range && move != i && board[i]==0)
				{
					//Randomly select between the two
					if(Math.Round(randInt.NextDouble(),0) == 1)  //then change to new
					{
						//set move
						move = i;
					}
				}
			}	


			state = "";

			//Debugging/////////////////////////////////111111111111111111111111111111111
			if(moves>5)
			{
				System.Windows.Forms.MessageBox.Show("AHHHH");
			}
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			
			moves++;
			//used to update the values, but need to get new state
			board[move] = this.player;
			newValue = findDPValue(board);
			this.previousState = "";
			for(int i = 0; i< 9; i++)
			{
				this.previousState = this.previousState.Insert(this.previousState.Length,board[i].ToString());
			}
			innerUpdate(newValue, this.previousState);
			return move;
		}
		
		private double findDPValue(int[] board)
		{
			string state;
			int[] opponentBoard;
			double newValue = 0;
			int count = 0;

			for(int i = 0; i<9; i++)
			{
				if(board[i] == 0)
				{
					//Get possible opponents moves
					opponentBoard = new int[9];
					for(int j = 0; j< 9; j++)
					{
						if(i != j)
						{
							opponentBoard[j] = board[j];
						}
						else
						{
							if(this.player == 1)
							{
								opponentBoard[j] = 2;
							}
							else //this.player ==2
							{
								opponentBoard[j] = 1;
							}
						}
					}
					//Now find current players possible moves 
					for(int k = 0; k<9; k++)
					{
						if(opponentBoard[k] == 0)
						{
							state = "";
							for(int m = 0; m< 9; m++)
							{
								//convert it to string
								if(k != m)
								{
									state = state.Insert(state.Length,opponentBoard[m].ToString());
								}
								else
								{
									if(this.player == 1)
									{
										state = state.Insert(state.Length,this.player.ToString());
									}
									else //this.player ==2
									{
										state = state.Insert(state.Length,this.player.ToString());
									}
								}
							}

							string state2 = (state[2].ToString() + state[5].ToString() + state[8].ToString() + state[1].ToString() + state[4].ToString() + state[7].ToString() + state[0].ToString() + state[3].ToString() + state[6].ToString());
							string state3  = (state[8].ToString() + state[7].ToString() + state[6].ToString() + state[5].ToString() + state[4].ToString() + state[3].ToString() + state[2].ToString() + state[1].ToString() + state[0].ToString());
							string state4 = (state[6].ToString() + state[3].ToString() + state[0].ToString() + state[7].ToString() + state[4].ToString() + state[1].ToString() + state[8].ToString() + state[5].ToString() + state[2].ToString());

							string state5 = (state[6].ToString() + state[7].ToString() + state[8].ToString() + state[3].ToString() + state[4].ToString() + state[5].ToString() + state[0].ToString() + state[1].ToString() + state[2].ToString());
							string state6 = (state[2].ToString() + state[1].ToString() + state[0].ToString() + state[5].ToString() + state[4].ToString() + state[3].ToString() + state[8].ToString() + state[7].ToString() + state[6].ToString());
							string state7 = (state[8].ToString() + state[5].ToString() + state[2].ToString() + state[7].ToString() + state[4].ToString() + state[1].ToString() + state[6].ToString() + state[3].ToString() + state[0].ToString());
							string state8 = (state[0].ToString() + state[3].ToString() + state[6].ToString() + state[1].ToString() + state[4].ToString() + state[7].ToString() + state[2].ToString() + state[5].ToString() + state[8].ToString());


							//find that state, add it up and increment count 
							bool found = false;
							for(int l = 0; l< MAX; l++)
							{
								if(state == this.states[0,l] || state2 == this.states[0,l] || state3 == this.states[0,l] || state4 == this.states[0,l]|| state5 == this.states[0,l]|| state6 == this.states[0,l]|| state7 == this.states[0,l]|| state8 == this.states[0,l])
								{
									found = true;
									newValue = newValue + double.Parse(this.states[1,l]);
									count++;
									l = MAX;
								}
							}
							if(found == false)
							{
								newValue = newValue + this.defaultValue;
								count++;
							}
						}
					}
				}
			}
			newValue = newValue/count;
			if(double.IsNaN(newValue))
			{
				newValue = this.defaultValue;
			}
			else if(double.IsPositiveInfinity(newValue) && count != 0)
			{
				newValue = 1.0;
			}
			else if(double.IsNegativeInfinity(newValue) && count != 0)
			{
				newValue = -1.0;
			}
			else if(count == 0)
			{
				newValue = 0;
			}

			return (newValue);
		}

		//update the xml file after the game
		public void update(int result, int[] board)
		{
			string currentState = "";
			//Assuming Board_SIZE = 9
			for(int i = 0; i < 9; i++)
			{
				currentState = currentState.Insert(currentState.Length,board[i].ToString());
			}
			//Read in value
			try
			{
				XmlTextReader DPstatesRead = new XmlTextReader("DPstates.xml");
				DPstatesRead.ReadStartElement("DynamicProgramming");
				int i = 0;
				this.states[0,i] = DPstatesRead.GetAttribute("string");
				this.states[1,i] = DPstatesRead.ReadString();
				i++;
				while(DPstatesRead.Read())
				{
					this.states[0,i] = DPstatesRead.GetAttribute("string");
					this.states[1,i] = DPstatesRead.ReadString();
					i++;
				}
				DPstatesRead.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}


			/*
			 * Optimization update for version 1.1 on 3/3/06
			 * 
			 */
			string currentState2 = (currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString());
			string currentState3 = (currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString());
			string currentState4 = (currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString());

			string currentState5 = (currentState[6].ToString() + currentState[7].ToString() + currentState[8].ToString() + currentState[3].ToString() + currentState[4].ToString() + currentState[5].ToString() + currentState[0].ToString() + currentState[1].ToString() + currentState[2].ToString());
			string currentState6 = (currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString());
			string currentState7 = (currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString());
			string currentState8 = (currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString());

		


			bool found;
			//update values
			found = false;
			for(int j = 0; j< MAX; j++)
			{
				/*
				 * Updated for optimization for versin 1.1 on 3/3/06
				 */
				if(currentState == this.states[0,j] || currentState2 == this.states[0,j] || currentState3 == this.states[0,j] || currentState4 == this.states[0,j] || currentState5 == this.states[0,j] || currentState6 == this.states[0,j] || currentState7 == this.states[0,j] || currentState8 == this.states[0,j])
				{
					found = true;
					if(result == 1)
					{
						this.states[1,j] = this.winReward.ToString();
					}
					else if(result == 0)
					{
						this.states[1,j] = this.tieReward.ToString();
					}
					else //result == -1
					{
						this.states[1,j] = this.lostReward.ToString();
					}
					j = MAX; //Exit loop
				}
			}
			//A new state is found
			if(found == false)
			{
				int location = 0;
				while(states[0,location] != null)
				{
					location++;
				}
				this.states[0,location] = currentState;
				if(result == 1)
				{
					this.states[1,location] = this.winReward.ToString();
				}
				else if(result == 0)
				{
					this.states[1,location] = this.tieReward.ToString();
				}
				else //result == -1
				{
					this.states[1,location] = this.lostReward.ToString();
				}
			}

			//System.Windows.Forms.MessageBox.Show("State: "+currentState+"\nReward: "+result);


			//write to xml
			XmlTextWriter DPstates = new XmlTextWriter("DPstates.xml",System.Text.Encoding.Default);
			DPstates.WriteStartDocument(true);
			DPstates.WriteStartElement("DynamicProgramming");
			int inc = 0;
			while(inc< MAX && states[0,inc] != null)
			{
				DPstates.WriteStartElement("state");
				DPstates.WriteAttributeString("string",states[0,inc]);
				DPstates.WriteString(states[1,inc]);
				DPstates.WriteEndElement();
				inc++;
			}
			DPstates.WriteEndElement();
			DPstates.WriteEndDocument();
			DPstates.Close();
		}

	}

	public class TemporalDifference
	{
		private double Epsilon = 0.1;
		private double StepSize = 0.1;
		private int winReward = 1;
		private int tieReward = 0;
		private int lostReward = -1;
		private bool autoExplore = true;
		private int outOfBoundsReward;
		private const int MAX_STATES = 5; 
		private double largestValue = -2.0;
		private int move;//represents the decision of where to move
		private int moves = 0;//represents the number of moves made;
		//Read/Write states in the array/file
		private const int MAX = 800;
		private string [,] states = new string[2,MAX];
		private int player;
		private double defaultValue = 2.0;
		private string previousState = "";

		public TemporalDifference(int player)
		{
			this.player = player;
			/*
			 * Read in the configuration
			 * 
			 */ 
			try
			{
				XmlTextReader TDconfig = new XmlTextReader("TDConfig.xml");
				
				TDconfig.ReadStartElement("TemporalDifference");
				TDconfig.ReadStartElement("Epsilon");
				Epsilon = double.Parse(TDconfig.ReadString());
				TDconfig.ReadEndElement();
				TDconfig.ReadStartElement("Stepsize");
				StepSize = double.Parse(TDconfig.ReadString());
				TDconfig.ReadEndElement();
				TDconfig.ReadStartElement("Reward");
				TDconfig.ReadStartElement("Win");
				winReward = int.Parse(TDconfig.ReadString());
				TDconfig.ReadEndElement();
				TDconfig.ReadStartElement("Tie");
				tieReward = int.Parse(TDconfig.ReadString());
				TDconfig.ReadEndElement();
				TDconfig.ReadStartElement("Loss");
				lostReward = int.Parse(TDconfig.ReadString());
				TDconfig.ReadEndElement();
				TDconfig.ReadEndElement();
				TDconfig.ReadStartElement("AutoExplore");
				string ans = TDconfig.ReadString();
				if(ans == "Yes")
				{
					autoExplore = true;
				}
				else //ans == no
				{
					autoExplore = false;
				}
				TDconfig.Close();

			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}

			outOfBoundsReward = winReward + 1;
			/*
			 * Read in the seen states
			 * 
			 * <?xml>
			 * <DynamicProgramming>
			 * <state string="?????????">value</state>
			 * </MonteCarlo>
			 * </xml>
			 */ 
			try
			{
				XmlTextReader TDstates = new XmlTextReader("TDstates.xml");
				TDstates.ReadStartElement("TemporalDifference");
				int i = 0;
				this.states[0,i] = TDstates.GetAttribute("string");
				this.states[1,i] = TDstates.ReadString();
				i++;
				while(TDstates.Read())
				{
					this.states[0,i] = TDstates.GetAttribute("string");
					this.states[1,i] = TDstates.ReadString();
					i++;
				}
				TDstates.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}
		}
		//find the best move and return it's answer.
		public void innerUpdate(double newValue, string currentState)
		{
			bool found = false;

			/*
			 * Optimized update for version 1.1 on 3/3/06
			 * 
			 * 
			 */
			string currentState2 = (currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString());
			string currentState3 = (currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString());
			string currentState4 = (currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString());

			string currentState5 = (currentState[6].ToString() + currentState[7].ToString() + currentState[8].ToString() + currentState[3].ToString() + currentState[4].ToString() + currentState[5].ToString() + currentState[0].ToString() + currentState[1].ToString() + currentState[2].ToString());
			string currentState6 = (currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString());
			string currentState7 = (currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString());
			string currentState8 = (currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString());

			for(int j = 0; j< MAX; j++)
			{
				/*
				 * Optimized update for version 1.1 on 3/3/06
				 */
				if(currentState == this.states[0,j] || currentState2 == this.states[0,j] || currentState3 == this.states[0,j] || currentState4 == this.states[0,j] || currentState5 == this.states[0,j] || currentState6 == this.states[0,j] || currentState7 == this.states[0,j] || currentState8 == this.states[0,j])
				{
					found = true;
					this.states[1,j] = Convert.ToString(double.Parse(this.states[1,j]) + this.StepSize*(newValue - double.Parse(this.states[1,j])));
					j = MAX; //Exit loop
				}
			}
			//A new state is found
			if(found == false)
			{
				int location = 0;
				//traverse through
				while(states[0,location] != null)
				{
					location++;
				}
				this.states[0,location] = currentState;
				this.states[1,location] = newValue.ToString();
			}

			//write to xml
			XmlTextWriter TDstates = new XmlTextWriter("TDstates.xml",System.Text.Encoding.Default);
			TDstates.WriteStartDocument(true);
			TDstates.WriteStartElement("TemporalDifference");
			int inc = 0;
			while(inc< MAX && states[0,inc] != null)
			{
				TDstates.WriteStartElement("state");
				TDstates.WriteAttributeString("string",states[0,inc]);
				TDstates.WriteString(states[1,inc]);
				TDstates.WriteEndElement();
				inc++;
			}
			TDstates.WriteEndElement();
			TDstates.WriteEndDocument();
			TDstates.Close();
		}

		//find the best move and return it's answer.
		public int getMove(int[] board, int BOARD_SIZE)
		{
			string currentState = "";
			double newValue = 0.0;
			double [] values = new double[9];

			//initialize the values to a low number
			for(int i = 0; i < BOARD_SIZE; i++)
			{
				values[i] = -100;
				currentState = currentState.Insert(currentState.Length,board[i].ToString());
			}

			//Is autoExplore on?
			if(this.autoExplore)
			{
				defaultValue = this.outOfBoundsReward;
			}
			else
			{
				// 1+0+(-1) = 0/3=0   0+(-1)+(-2) = -3/3=-1   1+(-1)+(-1) = -1/3 = -.333
				defaultValue = (this.winReward + this.tieReward + this.lostReward)/3;
			}
			

			//Find Best Move by Value
			this.largestValue = -2;
			string state;
			for(int i = 0; i<BOARD_SIZE; i++)
			{
				state = "";
				if(board[i] == 0)
				{
					//make string value of a possible state
					for(int k = 0; k<BOARD_SIZE; k++)
					{
						if(i != k)
						{
							state = state.Insert(state.Length,board[k].ToString());
						}
						else
						{
							state = state.Insert(state.Length,this.player.ToString());
						}
					}

					/*
					 * Optimized update for version 1.1 on 3/3/06
					 *  
					 */
					string state2 = (state[2].ToString() + state[5].ToString() + state[8].ToString() + state[1].ToString() + state[4].ToString() + state[7].ToString() + state[0].ToString() + state[3].ToString() + state[6].ToString());
					string state3 = (state[8].ToString() + state[7].ToString() + state[6].ToString() + state[5].ToString() + state[4].ToString() + state[3].ToString() + state[2].ToString() + state[1].ToString() + state[0].ToString());
					string state4 = (state[6].ToString() + state[3].ToString() + state[0].ToString() + state[7].ToString() + state[4].ToString() + state[1].ToString() + state[8].ToString() + state[5].ToString() + state[2].ToString());

					string state5 = (state[6].ToString() + state[7].ToString() + state[8].ToString() + state[3].ToString() + state[4].ToString() + state[5].ToString() + state[0].ToString() + state[1].ToString() + state[2].ToString());
					string state6 = (state[2].ToString() + state[1].ToString() + state[0].ToString() + state[5].ToString() + state[4].ToString() + state[3].ToString() + state[8].ToString() + state[7].ToString() + state[6].ToString());
					string state7 = (state[8].ToString() + state[5].ToString() + state[2].ToString() + state[7].ToString() + state[4].ToString() + state[1].ToString() + state[6].ToString() + state[3].ToString() + state[0].ToString());
					string state8 = (state[0].ToString() + state[3].ToString() + state[6].ToString() + state[1].ToString() + state[4].ToString() + state[7].ToString() + state[2].ToString() + state[5].ToString() + state[8].ToString());



					//find that state
					bool found = false;
					for(int j = 0; j< MAX; j++)
					{
						if(state == this.states[0,j] || state2 == this.states[0,j] || state3 == this.states[0,j] || state4 == this.states[0,j] || state5 == this.states[0,j]|| state6 == this.states[0,j]|| state7 == this.states[0,j]|| state8 == this.states[0,j])
						{
							found = true;
							values[i] = double.Parse(this.states[1,j]);
							if(values[i] > largestValue && board[i]==0)
							{
								largestValue = values[i];
								this.move = i;
							}
						}
					}
					if(found == false)
					{
						if(defaultValue > largestValue)
						{
							largestValue = defaultValue;
							this.move = i;
						}
						values[i] = defaultValue;
					}
				}
			}

			newValue = largestValue;

			//Check Epsilon Range
			double range = largestValue - this.Epsilon;
			Random randInt = new Random();
			//Find values within range and randomly select among them
			for(int i = 0; i < BOARD_SIZE; i++)
			{
				if(values[i] >= range && move != i && board[i]==0)
				{
					//Randomly select between the two
					if(Math.Round(randInt.NextDouble(),0) == 1)  //then change to new
					{
						//set move
						move = i;
						newValue = values[i];
					}
				}
			}	


			state = "";

			//Debugging/////////////////////////////////111111111111111111111111111111111
			if(moves>5)
			{
				System.Windows.Forms.MessageBox.Show("AHHHH");
			}
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			
			moves++;
			board[move] = this.player;
			if(this.previousState != "")
			{
				innerUpdate(newValue, this.previousState);
			}
			this.previousState = "";
			for(int i = 0; i< 9; i++)
			{
				this.previousState = this.previousState.Insert(this.previousState.Length,board[i].ToString());
			}

			return move;
		}
		//update the xml file after the game
		public void update(int result, int[] board)
		{
			string currentState = "";
			//Assuming Board_SIZE = 9
			for(int i = 0; i < 9; i++)
			{
				currentState = currentState.Insert(currentState.Length,board[i].ToString());
			}
			//Read in values
			try
			{
				XmlTextReader TDstatesRead = new XmlTextReader("TDstates.xml");
				TDstatesRead.ReadStartElement("TemporalDifference");
				int i = 0;
				this.states[0,i] = TDstatesRead.GetAttribute("string");
				this.states[1,i] = TDstatesRead.ReadString();
				i++;
				while(TDstatesRead.Read())
				{
					this.states[0,i] = TDstatesRead.GetAttribute("string");
					this.states[1,i] = TDstatesRead.ReadString();
					i++;
				}
				TDstatesRead.Close();
			}
			catch(System.IO.FileNotFoundException err)
			{
				//do nothing, use defaults
			}
			catch(Exception err)
			{
				System.Windows.Forms.MessageBox.Show(err.ToString(),"Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
			}

			bool found;

			/*
			 * Optimized update for version 1.1 on 3/3/06
			 * 
			 */
			string currentState2 = (currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString());
			string currentState3 = (currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString());
			string currentState4 = (currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString());

			string currentState5 = (currentState[6].ToString() + currentState[7].ToString() + currentState[8].ToString() + currentState[3].ToString() + currentState[4].ToString() + currentState[5].ToString() + currentState[0].ToString() + currentState[1].ToString() + currentState[2].ToString());
			string currentState6 = (currentState[2].ToString() + currentState[1].ToString() + currentState[0].ToString() + currentState[5].ToString() + currentState[4].ToString() + currentState[3].ToString() + currentState[8].ToString() + currentState[7].ToString() + currentState[6].ToString());
			string currentState7 = (currentState[8].ToString() + currentState[5].ToString() + currentState[2].ToString() + currentState[7].ToString() + currentState[4].ToString() + currentState[1].ToString() + currentState[6].ToString() + currentState[3].ToString() + currentState[0].ToString());
			string currentState8 = (currentState[0].ToString() + currentState[3].ToString() + currentState[6].ToString() + currentState[1].ToString() + currentState[4].ToString() + currentState[7].ToString() + currentState[2].ToString() + currentState[5].ToString() + currentState[8].ToString());

			//update values
			found = false;
			for(int j = 0; j< MAX; j++)
			{
				if(currentState == this.states[0,j] || currentState2 == this.states[0,j] || currentState3 == this.states[0,j] || currentState4 == this.states[0,j] || currentState5 == this.states[0,j] || currentState6 == this.states[0,j] || currentState7 == this.states[0,j] || currentState8 == this.states[0,j])
				{
					found = true;
					if(result == 1)
					{
						this.states[1,j] = this.winReward.ToString();
					}
					else if(result == 0)
					{
						this.states[1,j] = this.tieReward.ToString();
					}
					else //result == -1
					{
						this.states[1,j] = this.lostReward.ToString();
					}
					j = MAX; //Exit loop
				}
			}
			//A new state is found
			if(found == false)
			{
				int location = 0;
				while(states[0,location] != null)
				{
					location++;
				}
				this.states[0,location] = currentState;
				if(result == 1)
				{
					this.states[1,location] = this.winReward.ToString();
				}
				else if(result == 0)
				{
					this.states[1,location] = this.tieReward.ToString();
				}
				else //result == -1
				{
					this.states[1,location] = this.lostReward.ToString();
				}
			}

			//write to xml
			XmlTextWriter TDstates = new XmlTextWriter("TDstates.xml",System.Text.Encoding.Default);
			TDstates.WriteStartDocument(true);
			TDstates.WriteStartElement("TemporalDifference");
			int inc = 0;
			while(inc< MAX && states[0,inc] != null)
			{
				TDstates.WriteStartElement("state");
				TDstates.WriteAttributeString("string",states[0,inc]);
				TDstates.WriteString(states[1,inc]);
				TDstates.WriteEndElement();
				inc++;
			}
			TDstates.WriteEndElement();
			TDstates.WriteEndDocument();
			TDstates.Close();
		}

	}
}
